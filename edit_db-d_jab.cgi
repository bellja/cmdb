#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Create out headers and the Javascript functions
# we'll need.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
print ".fixed {font-size:12px}\n";
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(db_id, narrative_name, db_name, db_type, schema, path, node, env, server_id, state) {
	document.editFields.db_id.value = db_id;
	document.editFields.narrative_name.value = narrative_name;
	document.editFields.db_name.value = db_name;
	document.editFields.db_type.value = db_type;
	document.editFields.schema.value = schema;
	document.editFields.path.value = path;
	document.editFields.node.value = node;
	document.editFields.env[0].selected = true;
	document.editFields.server_id[0].selected = true;
	document.editFields.state[0].selected = true;
	for(i = 0; i < document.editFields.env.length; i++) {
		if (document.editFields.env[i].value == env) {
			document.editFields.env[i].selected = true;
		}
	}
        for(i = 0; i < document.editFields.server_id.length; i++) {
                if (document.editFields.server_id[i].value == server_id) {
                        document.editFields.server_id[i].selected = true;
                }
        }
        for(i = 0; i < document.editFields.state.length; i++) {
                if (document.editFields.state[i].value == state) {
                        document.editFields.state[i].selected = true;
                }
        }
}
function clearForm() {
	document.editFields.db_id.value = '';
	document.editFields.narrative_name.value = '';
	document.editFields.db_name.value = '';
	document.editFields.db_type.value = '';
	document.editFields.schema.value = '';
	document.editFields.path.value = '';
	document.editFields.node.value = '';
	document.editFields.env[0].selected = true;
	document.editFields.server_id[0].selected = true;
	document.editFields.state[0].selected = true;
}
function confirmDelete() {
        var answer = confirm(\"Delete this database record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}

\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_DB');

print "<b>Edit Database Table<b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing
statement: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$server[0]->{server_name} = "";
$server[0]->{server_id} = 0;
$server[0]->{env} = "";
while (($server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statemen
t: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;


if($q->param('server') eq "") {
	$cur_server = 0;
} else {
	$cur_server = $q->param('server_id');
}
$do_delete = $q->param('DELETE');

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print "<table>\n";
print "<tr>\n";
print $q->td(["Narrative Name:", $q->textfield(-name=>"narrative_name", -size=>50)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Database Name:", $q->textfield(-name=>"db_name")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Database Type:", $q->textfield(-name=>"db_type")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Schema:", $q->textfield(-name=>"schema")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Node:", $q->textfield(-name=>"node")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Path:", $q->textfield(-name=>"path")]);
print "</tr>\n";
print "<tr><td>\n";
print "Environment:\n";
print "</td><td>\n";
#print $q->popup_menu(-name=>"env", -values=>["", "PROD","SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"]);
print $q->popup_menu(-name=>"env", -values=>\@a_env);
print "</td></tr>\n";
print "<tr><td>\n";
print "Server:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
#print "<select name='server' class=fixed>\n";
print "<select name='server_id'>\n";
$s = 0;
$cur_server = $q->param('server_id');
for($i = 0; $i <= $#server; $i++) {
	print "<option value=\"$server[$i]->{server_id}\"";
	if($cur_server == $server[$i]->{server_id} && $s == 0) {
		print " selected";
		$s = 1;
	}
	print ">\n";
	if($i > 0 && length($server[$i]->{server_name}) > 0) {
		printf "%10.10s - %4.4s - %6.6s", $server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env};
	} else {
		print "&nbsp;"
	}
	print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr><td>\n";
print "State:\n";
print "</td><td>\n";
print $q->popup_menu(-name=>"state", -values=>["", "ASSIGNED","PLANNED","CONFIGURED","NOT USED","TEST"]);
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["DB ID:", $q->textfield(-name=>"db_id", -readonly=>"readonly")]);
print "</tr>\n";
print "</table>\n";
print "\n<br>\n";
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

if ($inputtype eq 'Search') {
	my $narrative_name = $q->param('narrative_name');
	my $db_name = $q->param('db_name');
	my $db_type = $q->param('db_type');
	my $schema = $q->param('schema');
	my $node = $q->param('node');
	my $path = $q->param('path');
	my $env = $q->param('env');
	my $server_id = $q->param('server_id');
	my $state = $q->param('state');
	my $search_string = "";
	my $a = 0;

	#
	# Protect against an undefined/null length
	# server_id.  Don't think it's necessary, but
	# better to be safe.
	#
	if(!defined($server_id) || $server_id eq "") {
		$server_id = 0;
	}

	#
	# Build while string
	#
	# Tell oracle to translate to all lower
	# case so that we're doing a case insensitive
	# search.
	#
	if(length($narrative_name) > 0) {
		$search_string = "lower(narrative_name) like lower('%$narrative_name%')";
		$a = 1;
	}
	if(length($db_name) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(db_name) like lower('%$db_name%')";
		} else {
			$search_string = "lower(db_name) like lower('%$db_name%')";
			$a = 1;
		}
	}
	if(length($db_type) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(db_type) like lower('%$db_type%')";
		} else {
			$search_string = "lower(db_type) like lower('%$db_type%')";
			$a = 1;
		}
	}
	if(length($schema) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(schema) like lower('%$schema%')";
		} else {
			$search_string = "lower(schema) like lower('%$schema%')";
			$a = 1;
		}
	}
	if(length($node) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(node) like lower('%$node%')";
		} else {
			$search_string = "lower(node) like lower('%$node%')";
			$a = 1;
		}
	}
	if(length($path) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(path) like lower('%$path%')";
		} else {
			$search_string = "lower(path) like lower('%$path%')";
			$a = 1;
		}
	}
	if($server_id > 0) {
		if($a) {
			$search_string = $search_string . " and server_id=$server_id";
		} else {
			$search_string = "server_id=$server_id";
			$a = 1;
		}
	}
	if(length($env) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(env) like lower('%$env%')";
		} else {
			$search_string = "lower(env) like lower('%$env%')";
			$a = 1;
		}
	}
	if(length($state) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(state) like lower('%$state%')";
		} else {
			$search_string = "lower(state) like lower('%$state%')";
			$a = 1;
		}
	}
	#
	# No need for a else, because is $a is still 0,
	# then search_string hasn't been changed and is
	# still length 0.
	#
	if($a > 0) {
		$search_string = " where " . $search_string;
	}
	$insstr = "select * from cmdb_db $search_string order by env, db_name, narrative_name";
print "<br>$insstr<br>\n";
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>DB_ID</th>\n";
	print "<th>DB Name</th>\n";
	print "<th>Narrative Name</th>\n";
	print "<th>DB Type</th>\n";
	print "<th>Schema</th>\n";
	print "<th>Node</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server</th>\n";
	print "<th>State</th>\n";
	print "<th>Path</th>\n";
	print "</tr>\n";
	while (($db_id, $narrative_name, $db_name, $db_type, $schema, $node, $env, $server_id, $state, $path) = $getTbl->fetchrow()) {
		if(length($narrative_name) > 0) {
			if(!defined($server_id)) {
				$server_id = 0;
			}
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm($db_id, '$narrative_name', '$db_name', '$db_type', '$schema', '$path', '$node', '$env', $server_id, '$state')\">$db_id</a></td>\n";
			if(length($db_name) < 1) { $db_name = "&nbsp;"; }
			print "<td>$db_name</td>\n";
			if(length($db_type) < 1) { $db_type = "&nbsp;"; }
			if(length($narrative_name) < 1) { $narrative_name = "&nbsp;"; }
			print "<td>$narrative_name</td>\n";
			print "<td>$db_type</td>\n";
			if(length($schema) < 1) { $schema = "&nbsp;"; }
			print "<td>$schema</td>\n";
			if(length($node) < 1) { $node = "&nbsp;"; }
			print "<td>$node</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name} - $server[$i]->{server_id}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			if($server_name eq " - 0") { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($state) < 1) { $state = "&nbsp;"; }
			print "<td>$state</td>\n";
			if(length($path) < 1) { $path = "&nbsp;"; }
			print "<td>$path</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
	

} elsif ($inputtype eq 'Create New') {
	my $narrative_name = $q->param('narrative_name');
	my $db_name = $q->param('db_name');
	my $db_type = $q->param('db_type');
	my $schema = $q->param('schema');
	my $node = $q->param('node');
	my $path = $q->param('path');
	my $env = $q->param('env');
	my $server_id = $q->param('server_id');
	my $state = $q->param('state');
    $db_name =~ s/^\s+|\s+$//g;

	if(!defined($server_id) || $server_id eq "") {
		$server_id = 0;
	}

    $insstr = "select server_id from cmdb_db where server_id='$server_id' and lower(db_name)=lower('$db_name') and env='$env'";
    $getTbl = $dbh->prepare($insstr);
    $getTbl->execute();
    $i = 0;
    while($this_db = $getTbl->fetchrow()) {
        $i++;
    }
    $getTbl->finish;

    if($i != 0) {
       print "<p><b>Duplicate entry ($server_id) detected.  No changes made to table.</b></p>\n";
       print $q->end_html();
       $dbh->disconnect;
       exit;
    }
    
	if(length($narrative_name) < 1 || length($db_type) < 1 || length($env) < 1 || $server_id == 0 || $server_id == "" || length($state) < 1) {
                print "<p><b>Error: Fields Narrative Name, DB Type, Environment, Server, and State must all have values.</b></p>\n";
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }


	$insstr = "insert into cmdb_db (narrative_name, db_name, db_type, schema, node, env, server_id, state, path) values ('$narrative_name', '$db_name', '$db_type', '$schema', '$node', '$env', $server_id, '$state', '$path')";

	print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert to CMDB_DB completed.</p>\n";
	} else {
		print "<p>Insert to CMDB_DB failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Update') {
	my $narrative_name = $q->param('narrative_name');
	my $db_name = $q->param('db_name');
	my $db_type = $q->param('db_type');
	my $schema = $q->param('schema');
	my $node = $q->param('node');
	my $path = $q->param('path');
	my $env = $q->param('env');
	my $server_id = $q->param('server_id');
	my $state = $q->param('state');
	my $db_id = $q->param('db_id');

	if(!defined($server_id) || $server_id eq "") {
		$server_id = 0;
	}

	if(length($narrative_name) < 1 || length($db_type) < 1 || length($env) < 1 || $server_id == 0 || $server_id == "" || length($state) < 1) {
                print "<p><b>Error: Fields Narrative Name, DB Type, Environment, Server, and State must all have values.</b></p>\n";
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }

	if($db_id < 1) {
		print"<p><b>DB_ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		$insstr = "update cmdb_db set db_name='$db_name', db_type='$db_type', schema='$schema', node='$node', path='$path', env='$env', server_id=$server_id, state='$state' where db_id=$db_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update to CMDB_DB completed.</p>\n";
		} else {
			print "<p>Update to CMDB_DB failed:</p>\n";
			print "<p>$insstr</p>\n";
		}
	}
} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $db_id = $q->param('db_id');
        if($db_id < 1 || $db_id == "") {
                print"<p><b>DB ID is zero.  Can't delete that.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select db_id from cmdb_db_uses where db_id=$db_id";
        $i = 0;
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        while($db_none = $getTbl->fetchrow()) {
            if ($db_none > 0) {
                $i++;
            }
        }

        $getTbl->finish;
        if($i != 0) {
            print "<p><b>A matching DB ID still exists in the USES table. No changes can be made at this time.</b></p>\n";
            print $q->end_html();
            $dbh->disconnect;
            exit;
        }

        $insstr = "delete from cmdb_db where db_id=$db_id";

        print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_DB completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_DB failed:</p>\n";
                print "<p>$insstr</p>\n";
        }

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;
exit 0;
