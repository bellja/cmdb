#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Print the headers, and create the two Javascript
# functions we use.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(server_id, server_name, env, os, version) {
	document.editFields.env[0].selected = true;
	document.editFields.os[0].selected = true;
	document.editFields.server_id.value = server_id;
	document.editFields.server.value = server_name;
	document.editFields.env.value = env;
	document.editFields.os.value = os;
	document.editFields.os_version.value = version;
}
function clearForm() {
	document.editFields.server_id.value = '';
	document.editFields.server.value = '';
	document.editFields.os_version.value = '';
	document.editFields.env[0].selected = true;
	document.editFields.os[0].selected = true;
}
function confirmDelete() {
        var answer = confirm(\"Delete this server record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}
\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_SERVER');

print "<b>Edit Server Table</b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# inputtype holds the value of the submit button
# that was clicked, either Search, Update or
# Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');

#
# Get a list of OS names
#
$getTbl = $dbh->prepare("select unique os from cmdb_server order by os") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$i = 0;
while ($a_os[$i] = $getTbl->fetchrow()) {
	$i++;
}
#
# We need it in a hash, not an array, so
# map it into a hash
#
%h_os = map { $_, $_ } @a_os;
$getTbl->finish;

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
	$i++;
}
$getTbl->finish;

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print $q->table(
	$q->Tr(
	[
		$q->td(["Server Name:", $q->textfield(-name=>"server")]),
		#$q->td(["Environment:", $q->popup_menu(-name=>"env", -values=>["PROD", "SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"])]),
		$q->td(["Environment:", $q->popup_menu(-name=>"env", -values=>\@a_env)]),
		$q->td(["Operating System:", $q->popup_menu(-name=>"os", -values=>\%h_os)]),
		$q->td(["OS Version:", $q->textfield(-name=>"os_version")]),
		$q->td(["Server ID:", $q->textfield(-name=>"server_id", -readonly=>"readonly", -value=>"")])
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";

# If $q->param() equals "", this is the first time through.
$inputtype = $q->param('inputtype');
#if ($q->param() ne "") {
if ($inputtype eq 'Search') {
	my $server = $q->param('server');
	my $env = $q->param('env');
	my $os = $q->param('os');
	my $os_version = $q->param('os_version');
	#
	# Build while string
	#
	$a = 0;
	if(length($env) > 0) {
		$search_string = "env='$env'";
		$a = 1;
	}
	if(length($server) > 0) {
		$server  =~ s/ /%/g;
		if($a) {
			$search_string = $search_string . " and lower(server_name) like lower('%$server%')"
		} else {
			$search_string = $search_string . " lower(server_name) like lower('%$server%')";
			$a = 1;
		}
	}
	if(length($os) > 0) {
		$os =~ s/ /%/g;
		if($a) {
			$search_string = $search_string . " and lower(os) like lower('%$os%')";
		} else {
			$search_string = " lower(os) like lower('%$os%')";
			$a = 1;
		}
	}
	if(length($os_version) > 0) {
		$os_version =~ s/ /%/g;
		if($a) {
			$search_string = $search_string . " and lower(os_version) like lower('%$os_version%')";
		} else {
			$search_string = " lower(os_version) like lower('%$os_version%')";
			$a = 1;
		}
	}
	if($a) {
		$search_string = " where " . $search_string;
	} else {
		$search_string = "";
	}
        $getTbl = $dbh->prepare("select * from cmdb_server $search_string order by env, server_name") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Server ID</th>\n";
	print "<th>Server Name</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Operating System</th>\n";
	print "<th>OS Version</th>\n";
	print "</tr>\n";
	while (($server_id, $server_name, $env, $os, $os_version) = $getTbl->fetchrow()) {
		if(length($server_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm($server_id, '$server_name', '$env', '$os', '$os_version')\">$server_id</a></td>\n";
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			#
			# If you put an data element in with no value,
			# ie. "<td></td>" it messes up the way the 
			# table looks, so if the os length is zero,
			# put a non-breakable space in to make a real
			# table cell.
			#
			if(length($os) > 0) {
				print "<td>$os</td>\n";
			} else {
 				print "<td>&nbsp</td>\n";
			}
			if(length($os_version) > 0) {
				print "<td>$os_version</td>\n";
			} else {
				print "<td>&nbsp</td>\n";
			}
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	my $server = $params{'server'};
	my $env = $params{'env'};
	my $os = $params{'os'};
	my $os_version = $params{'os_version'};
	my $i;
	my $temp;
	my $insstr;

	if(length($server) < 1 || length($env) < 1 || length($os) < 1) {
                print "<p><b>Error: You must enter a server name, an environment and an OS.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }

        $insstr = "select server_name from cmdb_server where server_name='$server' and env='$env' and os='$os'";
	#print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>Duplicate entry detected.  No change made to table.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select server_name from cmdb_server where server_name='$server' and env='$env'";
	#print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>Server name and environment must be unique.  No change made to table.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

	$insstr = "insert into cmdb_server (server_name, env, os, os_version) values ('$server', '$env', '$os', '$os_version')";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_SERVER completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_SERVER failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Update') {
	my $server = $params{'server'};
	my $env = $params{'env'};
	my $os = $params{'os'};
	my $os_version = $params{'os_version'};
	my $server_id = $params{'server_id'};
	my $i;
	if($server_id < 1) {
		print"<p><b>Server ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
	}
	if(length($env) < 1) {
		print"<p><b>ENV is required.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
	}
	$insstr = "select env from cmdb_server where server_id=$server_id";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $temp = $getTbl->fetchrow();
        $getTbl->finish;
        #if($i != 0) {
        if($temp ne $env) {
		$insstr = "select comp_env_id from cmdb_comp_env where server_id=$server_id";
		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		$i = 0;
		while ($temp = $getTbl->fetchrow()) {
			$i++;
		}
		$insstr = "select db_id from cmdb_db where server_id=$server_id";
		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		while ($temp = $getTbl->fetchrow()) {
			$i++;
		}
		$insstr = "select vp_id from cmdb_vip_port where server_id=$server_id";
		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		while ($temp = $getTbl->fetchrow()) {
			$i++;
		}
		if($i == 0) {
			$insstr = "update cmdb_server set server_name='$server', env='$env', os='$os', os_version='$os_version' where server_id=$server_id";

			#print "<br>$insstr<br>\n";

			$getTbl = $dbh->prepare($insstr);
			$i = $getTbl->execute();
			$getTbl->finish;
			if($i != 0) {
				print "<p>Update to CMDB_SERVER completed.</p>\n";
			} else {
				print "<p>Update to CMDB_SERVER failed:</p>\n";
				print "<p>$insstr</p>\n";
			}
			print $q->end_form();
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}
        } else {
		$insstr = "update cmdb_server set server_name='$server', env='$env', os='$os', os_version='$os_version' where server_id=$server_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update to CMDB_SERVER completed.</p>\n";
		} else {
			print "<p>Update to CMDB_SERVER failed:</p>\n";
			print "<p>$insstr</p>\n";
		}
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
	}
	if($i != 0) {
		print "<p><b>You have changed the environment and this server\n";
		print "has an entry in one or more of  the tables CMDB_VIP_PORT,\n";
		print "CMDB_DB, and CMDB_COMP_ENV.  You must click on the\n";
		print "button below to complete the update.</b></p>\n";
		print $q->submit(-value => "Continue Update", -name => "inputtype");
		print "\n";
	}

} elsif ($inputtype eq 'Continue Update') {
	$server = $params{'server'};
	$env = $params{'env'};
	$os = $params{'os'};
	$os_version = $params{'os_version'};
	$server_id = $params{'server_id'};
	if($server_id < 1) {
		print"<p><b>Server ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		$insstr = "update cmdb_server set server_name='$server', env='$env', os='$os', os_version='$os_version' where server_id=$server_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update to CMDB_SERVER completed.</p>\n";
		} else {
			print "<p>Update to CMDB_SERVER failed:</p>\n";
			print "<p>$insstr</p>\n";
		}
	}
} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $server_id = $q->param('server_id');
        if($server_id < 1 || $server_id == "") {
                print"<p><b>Server ID is zero.  Can't delete that.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select comp_env_id from cmdb_comp_env where server_id=$server_id";
	#print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>This server ID is still in use in the CMDB_COMP_GEN table.  No changes made to CMDB_SERVER table</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select db_id from cmdb_db where server_id=$server_id";
	#print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>This server ID is still in use in the CMDB_DB table.  No changes made to CMDB_SERVER table</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select vp_id from cmdb_vip_port where server_id=$server_id";
	#print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>This server ID is still in use in the CMDB_VIP_PORT table.  No changes made to CMDB_SERVER table</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "delete from cmdb_server where server_id=$server_id";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
		print "<p>Delete from CMDB_SERVER completed.</p>\n";
        } else {
		print "<p>Delete from CMDB_SERVER failed:</p>\n";
		print "<p>$insstr</p>\n";
        }

}
print $q->end_form();
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;

