#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: what-db-on-server.cgi                                            #
# Author: Herb Gregory                                                     #
# Date: Aug 16,2011                                                       #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get all Server Environment pairs that have at least 1 DB/File System

my $getServers = $dbh->prepare("
	      SELECT UNIQUE cmdb_server.server_name, cmdb_server.env
	      FROM cmdb_server, cmdb_db
	      WHERE cmdb_server.server_id = cmdb_db.server_id
	      ORDER BY env
")
    or die "Error preparing getServers statement: $DBI::errstr\n";

# Get specific Server Environment pair

my $getServer = $dbh->prepare("
	      SELECT count(*)
	      FROM cmdb_server
	      WHERE cmdb_server.server_name = ? and cmdb_server.env = ?
")
    or die "Error preparing getServer statement: $DBI::errstr\n";

# Get Database information from the cmdb_db table

my $getdbServer = $dbh->prepare("
		SELECT  cmdb_db.db_name, cmdb_db.narrative_name, cmdb_db.db_type, cmdb_db.schema,
			cmdb_db.node, cmdb_db.server_id, cmdb_db.state
		FROM    cmdb_db, cmdb_server
		WHERE   cmdb_server.server_id=cmdb_db.server_id
		AND     cmdb_server.server_name = ? and cmdb_server.env = ?
")
    or die "Error preparing getdbServer statement: $DBI::errstr\n";

# Get Server name based on Server Id

my $getServerName = $dbh->prepare("
		  SELECT  cmdb_server.server_name
		  FROM    cmdb_server
		  WHERE   cmdb_server.server_id = ?
")
    or die "Error preparing getServerName statement: $DBII::errstr\n";

use strict;
#use warnings;          # Turned this off to avoid warnings about unitialized variables for NULL columns
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my $count         = "";
my $servername    = "";
my $server        = "";
my $env           = "";
my $dbName        = "";
my $narrativeName = "";
my $dbType        = "";
my $schema        = "";
my $node          = "";
my $state         = "";
my $serverId      = "";
my $serverName    = "";

###############################################
# Main                                        #
###############################################


# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What components are on server: ";
    print $q->textfield(-name => "se");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();

# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

    # Gather and output the list of current unique Language Version pairs

    print "Current list of Server Environment pairs with at least 1 DB/File System:";
    print $q->br;

    $getServers->execute();
    while (($server, $env) = $getServers->fetchrow()) {
	print $q->br;
	print "$server $env";
    }
    $getServers->finish;

} else {                                # Second time thru - get info for the specified Server Env

    my $serverenvironment = uc($q->param('se'));
    # Parse the Server Environment string
    $_ = $serverenvironment;
    $env = $1 if (s/ (\S+)$//);
    $server = $_;

    # Verify Server Environment pair is valid
    $getServer->execute($server, $env);
    $count = $getServer->fetchrow();
    $getServer->finish;

    if ($count == 0) {
	print "Server $server Environment $env not found\n";
	print "Processing stopped\n";
	exit 1;
    }

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>What DBs are on $server $env</title></head>";

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    print "<TR><TH> DB_NAME </TH><TH> NARRATIVE_NAME </TH><TH> DB_TYPE </TH><TH> SCHEMA </TH><TH> NODE </TH><TH> STATE </TH><TH> SERVER_ID </TH><TH> SERVER_NAME </TH></TR>";

    $getdbServer->execute($server, $env);

    while (($dbName, $narrativeName, $dbType, $schema, $node, $serverId, $state) = $getdbServer->fetchrow()) {

	    # Get Server Name
	    $getServerName->execute($serverId);
	    ($serverName) = $getServerName->fetchrow();
	    $getServerName->finish;

	    print "<TR><TD> $dbName </TD>";
	    print "<TD> $narrativeName </TD>";
	    print "<TD> $dbType </TD>";
	    print "<TD> $schema </TD>";
	    print "<TD> $node </TD>";
	    print "<TD> $state </TD>";
	    print "<TD> $serverId </TD>";
	    print "<TD> $serverName </TD>";

	    # Clear variables
	    $dbName        = "";
	    $narrativeName = "";
	    $dbType        = "";
	    $schema        = "";
	    $node          = "";
	    $state         = "";
	    $serverId      = "";
	    $serverName    = "";
    }
    $getdbServer->finish;
}


###############################################
# Closing                                     #
###############################################

#$dbh->commit();

$dbh->disconnect;

exit 0;

