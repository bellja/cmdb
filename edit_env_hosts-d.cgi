#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Print the headers, and create the two Javascript
# functions we use.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(host, env) {
	document.editFields.hosts.value = host;
	document.editFields.env.value = env;
}
function clearForm() {
	document.editFields.hosts.value = '';
	document.editFields.env.value = '';
}
function confirmDelete() {
        var answer = confirm(\"Delete this host environment record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}

\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_ENV_HOSTS');

print "<b>Edit Enrironment Hosts Table</b><br>\n";

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statemen
t: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# inputtype holds the value of the submit button
# that was clicked, either Search, Update or
# Create New.
#
$inputtype = $q->param('inputtype');

#
# Get a list of hostnames and their enviorment
#
$getTbl = $dbh->prepare("select unique env, hosts from cmdb_env_hosts order by env") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$i = 0;
while ($a_os[$i] = $getTbl->fetchrow()) {
	$i++;
}
#
# We need it in a hash, not an array, so
# map it into a hash
#
%h_os = map { $_, $_ } @a_os;
$getTbl->finish;
$do_delete = $q->param('DELETE');

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print $q->table(
	$q->Tr(
	[
		$q->td(["Host Description:", $q->textfield(-size=>80, -name=>"hosts")]),
		$q->td(["Environment:", $q->textfield(-size=>8, -name=>"env")]),
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

$inputtype = $q->param('inputtype');
if ($inputtype eq 'Search') {
	my $host = $q->param('hosts');
	my $env = $q->param('env');
	my $a = 0;
	#
	# Build while string
	#
	if(length($env) > 0) {
		$search_string = "lower(env) like lower('%$env%')";
		$a = 1;
	}
	if(length($host) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(hosts) like lower('%$host%')"
		} else {
			$search_string = $search_string . " lower(hosts) like lower('%$host%')"
		}
	}
	if($a) {
		$search_string = "where " . $search_string;
	} else {
		$search_string = "";
	}
	$insstr = "select * from cmdb_env_hosts $search_string order by env, hosts";
        $getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Environment</th>\n";
	print "<th>Host Description</th>\n";
	print "</tr>\n";
	while (($env, $host) = $getTbl->fetchrow()) {
		if(length($env) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm('$host', '$env')\">$env</a></td>\n";
			if(length($host) < 1) { $host = "&nbsp"; }
			print "<td>$host</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	$host = $params{'hosts'};
	$env = $params{'env'};

	$insstr = "select env from cmdb_env_hosts where env='$env'";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while($this_env = $getTbl->fetchrow()) {
		$i++;
	}
	$getTbl->finish;
	if($i != 0) {
		print "<p><b>Duplicate ENV ($env) detected.  No changes made to table.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}
	if(length($env) < 1) {
		print"<p><b>Environment field is required.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	} elsif(length($host) < 1) {
		print"<p><b>Host description field is required.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "insert into cmdb_env_hosts (hosts, env) values ('$host', '$env')";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = 0;
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_ENV_HOSTS completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_ENV_HOSTS failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Update') {
	$host = $params{'hosts'};
	$env = $params{'env'};
	$server_id = $params{'server_id'};
	if(length($env) < 1) {
		print"<p><b>No environment?  Perhaps you meant to create a new record?</b></p>\n";
	} elsif(length($host) < 1) {
		print"<p><b>Host description field is required.</b></p>\n";
	} else {
		$insstr = "update cmdb_env_hosts set hosts='$host' where env='$env'";

		print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update to CMDB_ENV_HOSTS completed.</p>\n";
		} else {
			print "<p>Update to CMDB_ENV_HOSTS failed:</p>\n";
			print "<p>$insstr</p>\n";
		}
	}
} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $env = $params{'env'};
        if(length($env) < 1) {
                print"<p><b>Environment is null.  Can't delete that.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "delete from cmdb_env_hosts where env='$env'";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_ENV_HOSTS completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_ENV_HOSTS failed:</p>\n";
                print "<p>$insstr</p>\n";
        }

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;
