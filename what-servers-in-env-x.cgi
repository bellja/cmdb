#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: what-servers-in-env-x.cgi                                        #
# Author: Herb Gregory                                                     #
# Date: Dec 2, 2011                                                        #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get Env and Hosts infor for all environments

my $getEnvHosts = $dbh->prepare("
		SELECT  cmdb_env_hosts.env, cmdb_env_hosts.hosts
		FROM    cmdb_env_hosts
		ORDER BY env
")
    or die "Error preparing getEnvHosts statement: $DBI::errstr\n";

# Get Host info for the specified Environment

my $getHost = $dbh->prepare("
		  SELECT  cmdb_env_hosts.hosts
		  FROM    cmdb_env_hosts
		  WHERE   upper(cmdb_env_hosts.env) = ?
")
    or die "Error preparing getHost statement: $DBI::errstr\n";

# Get F5 for the specified Environment

my $getF5 = $dbh->prepare("
		  SELECT  cmdb_server.server_name, cmdb_server.server_id, cmdb_server.os
		  FROM    cmdb_server
		  WHERE   upper(cmdb_server.server_name) like upper('F5%') and cmdb_server.env = ?
		  ORDER BY os
")
    or die "Error preparing getF5 statement: $DBI::errstr\n";

# Get Server Name and Server ID and Server OS for the specified Environment

my $getServerName = $dbh->prepare("
		  SELECT  cmdb_server.server_name, cmdb_server.server_id, cmdb_server.os
		  FROM    cmdb_server
		  WHERE   upper(cmdb_server.env) = ?
		  ORDER BY os, server_name
")
    or die "Error preparing getServerName statement: $DBI::errstr\n";

# Get the count of Components associated with a specific Server ID

my $getCompCount = $dbh->prepare("
		  SELECT  count(*)
		  FROM    cmdb_comp_env
		  WHERE   cmdb_comp_env.server_id = ?
")
    or die "Error preparing getCompCount statement: $DBI::errstr\n";

my $getDBCount = $dbh->prepare("
		  SELECT  count(*)
		  FROM    cmdb_db
		  WHERE   cmdb_db.server_id = ?
")
    or die "Error preparing getDBCount statement: $DBI::errstr\n";

use strict;
#use warnings;          # Turned this off to avoid warnings about unitialized variables for NULL columns
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my $env         = "";
my $hosts       = "";
my $serverName  = "";
my $serverId    = "";
my $os = "";
my $count = "";

###############################################
# Main                                        #
###############################################


# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What environment do you want information for: ";
    print $q->textfield(-name => "env");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();

# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

    # Gather and output the list of current Environments and what they are Hosting

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>Environment Query</title></head>";

    print "Current list of Environments:";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> ENVIRONMENT </TH><TH> HOSTS </TH></TR>";

    $getEnvHosts->execute();
    while (($env, $hosts) = $getEnvHosts->fetchrow()) {
       # Output one web page row
	print "<TR><TD> $env </TD>";
	print "<TD> $hosts </TD></TR>";

	# Clear variables for next pass
	$env = "";
	$hosts = "";

    }# Process Environment info for the specified Environment
    $getEnvHosts->finish;

} else {                          # Second time thru - get info for the specified environment

    my $env = uc($q->param('env'));

    ########## Output Environment and Hosts info ##########

    $getHost->execute($env);
    ($hosts) = $getHost->fetchrow();
    $getHost->finish;

    if ($hosts eq "") {
	    print "Environment $env not found in cmdb_env_host table\n";
	    print "Processing stopped\n";
	    exit 1;
    }


    # Output the browser header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>Information for ESOps environment $env</title></head>";

    print "ENVIRONMENT HOST INFORMATION";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> ENVIRONMENT </TH><TH> HOSTS </TH></TR>";

    print "<TR><TD> $env </TD>";
    print "<TD> $hosts </TD></TR>";
    print "</TABLE CLASS>";
    print $q->br;               # Blank line between tables

    ########## Process F5 Router info for the specified Environment ##########

    print "F5 INFO";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> F5 </TH><TH> OS </TH></TR>";

    $getF5->execute($env);

    if (($serverName, $serverId, $os) = $getF5->fetchrow()) {

	    print "<TR><TD> $serverName </TD>";
	    print "<TD> $os </TD></TR>";

    }
    print "</TABLE CLASS>";
    $getF5->finish;
    print $q->br;

    ######### Process Servers in  OS order: Linux, Solaris then Windows #########
    # Only output info for servers that have Components associated with them

    print "COMPONENT INFO";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> SERVER </TH><TH> OS </TH></TR>";

    $getServerName->execute($env);

    while (($serverName, $serverId, $os) = $getServerName->fetchrow()) {

	    # See if there are any Components associated with this server
	    $getCompCount->execute($serverId);
	    ($count) = $getCompCount->fetchrow();
	    $getCompCount->finish;

	    if ($count > 0) {
		# Output Server info
		print "<TR><TD> $serverName </TD>";
		print "<TD> $os </TD></TR>";
	    }
    }
    print "</TABLE CLASS>";
    print $q->br;
    $getServerName->finish;

    ######### REPEAT ABOVE STEPS FOR DATABASES ###########

    # Process Servers in  OS order: Linux, Solaris then Windows
    # Only output info for servers that have Databases associated with them

    print " DATABASE INFO\n";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> SERVER </TH><TH> OS </TH></TR>";

    $getServerName->execute($env);

    while (($serverName, $serverId, $os) = $getServerName->fetchrow()) {

	    # See if there are any Databases associated with this server
	    $getDBCount->execute($serverId);
	    ($count) = $getDBCount->fetchrow();
	    $getDBCount->finish;

	    if ($count > 0) {
		# Output Server info
		print "<TR><TD> $serverName </TD>";
		print "<TD> $os </TD></TR>";
	    }


    }
    $getServerName->finish;
    print "</TABLE CLASS>";
}


###############################################
# Closing                                     #
###############################################

#$dbh->commit();


$dbh->disconnect;

exit 0;

