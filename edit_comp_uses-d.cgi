#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Create out headers and the Javascript functions
# we'll need.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(side, comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location) {
	if(side == \"L\") {
		document.editFields.l_name.value = '';
		document.editFields.l_type[0].selected = true;
		document.editFields.l_system_id.value = '';
		document.editFields.l_env[0].selected = true;
		document.editFields.l_server[0].selected = true;
		document.editFields.l_app_server.value = '';
		document.editFields.l_env_id.value = '';
		document.editFields.l_deploy_location.value = '';
		document.editFields.l_name.value = comp_name;
		for(i = 0; i < document.editFields.l_type.length; i++) {
			if (document.editFields.l_type[i].value == comp_type) {
				document.editFields.l_type[i].selected = true;
			}
		}
		document.editFields.l_system_id.value = system_id;
		for(i = 0; i < document.editFields.l_env.length; i++) {
			if (document.editFields.l_env[i].value == env) {
				document.editFields.l_env[i].selected = true;
			}
		}
		for(i = 0; i < document.editFields.l_server.length; i++) {
			if (document.editFields.l_server[i].value == server_id) {
				document.editFields.l_server[i].selected = true;
			}
		}
		document.editFields.l_app_server.value = app_server;
		document.editFields.l_env_id.value = comp_env_id;
		document.editFields.l_deploy_location.value = deploy_location;
	} else {
		document.editFields.r_name.value = '';
		document.editFields.r_type[0].selected = true;
		document.editFields.r_system_id.value = '';
		document.editFields.r_env[0].selected = true;
		document.editFields.r_server[0].selected = true;
		document.editFields.r_app_server.value = '';
		document.editFields.r_env_id.value = '';
		document.editFields.r_deploy_location.value = '';
		document.editFields.r_name.value = comp_name;
		for(i = 0; i < document.editFields.r_type.length; i++) {
			if (document.editFields.r_type[i].value == comp_type) {
				document.editFields.r_type[i].selected = true;
			}
		}
		document.editFields.r_system_id.value = system_id;
		for(i = 0; i < document.editFields.r_env.length; i++) {
			if (document.editFields.r_env[i].value == env) {
				document.editFields.r_env[i].selected = true;
			}
		}
		for(i = 0; i < document.editFields.r_server.length; i++) {
			if (document.editFields.r_server[i].value == server_id) {
				document.editFields.r_server[i].selected = true;
			}
		}
		document.editFields.r_app_server.value = app_server;
		document.editFields.r_env_id.value = comp_env_id;
		document.editFields.r_deploy_location.value = deploy_location;
	}
}
function clearForm(side) {
	if(side == \"L\") {
		document.editFields.l_name.value = '';
		document.editFields.l_type[0].selected = true;
		document.editFields.l_system_id.value = '';
		document.editFields.l_env[0].selected = true;
		document.editFields.l_server[0].selected = true;
		document.editFields.l_app_server.value = '';
		document.editFields.l_env_id.value = '';
		document.editFields.l_deploy_location.value = '';
	} else {
		document.editFields.r_name.value = '';
		document.editFields.r_type[0].selected = true;
		document.editFields.r_system_id.value = '';
		document.editFields.r_env[0].selected = true;
		document.editFields.r_server[0].selected = true;
		document.editFields.r_app_server.value = '';
		document.editFields.r_env_id.value = '';
		document.editFields.r_deploy_location.value = '';
	}
}
function confirmDelete() {
	if(document.editFields.l_env_id.value == 0 || document.editFields.l_env_id.value == '' || document.editFields.r_env_id.value == 0 || document.editFields.r_env_id.value == '') {
		alert(\"A valid ENV ID must exist in both tables.\");
	} else {
		var answer = confirm(\"Delete this uses/includes record?\");
		if (answer){
			document.editFields.inputtype.value = \"Delete\";
			document.editFields.DELETE.value = \"yes\";
			document.editFields.submit();
		}
	}
}
function checkFindUsedBy() {
	if(document.editFields.l_env_id.value == 0 || document.editFields.l_env_id.value == '') {
		alert(\"No valid Env ID.  Can't Continue.\");
	} else {
		document.editFields.inputtype.value = \"Find Used By\";
		document.editFields.submit();
	}
}
\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit USES/INCLUDES');

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing
statement: $DBI::errstr\n";
$getTbl->execute();
$server[0]->{server_name} = "";
$server[0]->{server_id} = 0;
$server[0]->{env} = "";
$i = 1;
while (($server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statemen
t: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

print "<b>Edit Uses/Includes Table</b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');
$startas = $q->param('startas');
$startas = $q->param('startas');

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print "<table border=1>\n";
print "<tr>\n";
print "<td>\n";
print "<table>\n";
print "<tr>\n";
print $q->td(["Name:", $q->textfield(-name=>"l_name", -size=>30)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Type:", $q->popup_menu(-name=>"l_type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["System ID:", $q->textfield(-name=>"l_system_id")]);
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Environment:\n";
print "</td>\n";
print "<td>\n";
#print $q->popup_menu(-name=>"l_env", -values=>["", "PROD","SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"]);
print $q->popup_menu(-name=>"l_env", -values=>\@a_env);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Server ID:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='l_server' class=fixed>\n";
#print "<select name='l_server_id'>\n";
$s = 0;
$l_server = $q->param('l_server');
for($i = 0; $i <= $#server; $i++) {
        print "<option value=\"$server[$i]->{server_id}\"";
        if($l_server == $server[$i]->{server_id} && $s == 0) {
                print " selected";
                $s = 1;
        }
        print ">\n";
        if($i > 0 && length($server[$i]->{server_name}) > 0) {
                printf "%s", $server[$i]->{server_name};
        } else {
                print "&nbsp;"
        }
        print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Deploy Location:", $q->textfield(-name=>"l_deploy_location", -size=>"55")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["App Server:", $q->textfield(-name=>"l_app_server")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Env ID:", $q->textfield(-name=>"l_env_id", -readonly=>"readonly")]);
print "</tr>\n";
print "<tr><td colspan=2>\n";
print "<table>\n";
print "<tr>\n";
print "<td>\n";
print "\n<input type=\"button\" onClick='clearForm(\"L\");' value=\"Clear Fields\" /></input>\n";
print "</td>\n";
print "<td>\n";
#print "&nbsp;";
print $q->submit(-value => "Search", -name => "l_search");
#print "<br>";
print "</td>\n";
print "</tr><tr>\n";
print "<td colspan=2>\n";
print "Find items in CMDB_COMP_USES/CMDB_INCLUDES which are used by/included by this Env ID.<br>\n";
print "</td></tr><tr>\n";
print "<td>\n";
print $q->submit(-value => "Find Used By", -name => "inputtype");
#print "\n<input type=\"button\" onClick=\"checkFindUsedBy();\" value=\"Find Used By\" name=\"inputtype\" /></input><br>\n";
#print "&nbsp;";
print "</td>\n";
print "<td>\n";
print $q->submit(-value => "Find Included By", -name => "inputtype");
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</td>\n";
print "<td align=center>\n";
print "&nbsp;\n";
print "&nbsp;\n";
#print $q->popup_menu(-name=>"uses", -values=>["uses", "used by", "includes", "included by"]);
if($startas eq "includes") {
	print $q->popup_menu(-name=>"uses", -values=>["includes", "uses"]);
} else {
	print $q->popup_menu(-name=>"uses", -values=>["uses", "includes"]);
}
print "&nbsp;\n";
print "&nbsp;\n";
print "</td>\n";
print "<td>\n";
print "<table>\n";
print "<tr>\n";
print $q->td(["Name:", $q->textfield(-name=>"r_name", -size=>30)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Type:", $q->popup_menu(-name=>"r_type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["System ID:", $q->textfield(-name=>"r_system_id")]);
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Environment:\n";
print "</td>\n";
print "<td>\n";
print $q->popup_menu(-name=>"r_env", -values=>\@a_env);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Server ID:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='r_server' class=fixed>\n";
#print "<select name='r_server_id'>\n";
$s = 0;
$r_server = $q->param('r_server');
for($i = 0; $i <= $#server; $i++) {
        print "<option value=\"$server[$i]->{server_id}\"";
        if($r_server == $server[$i]->{server_id} && $s == 0) {
                print " selected";
                $s = 1;
        }
        print ">\n";
        if($i > 0 && length($server[$i]->{server_name}) > 0) {
                printf "%s", $server[$i]->{server_name};
        } else {
                print "&nbsp;"
        }
        print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Deploy Location:", $q->textfield(-name=>"r_deploy_location", -size=>"55")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["App Server:", $q->textfield(-name=>"r_app_server")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Env ID:", $q->textfield(-name=>"r_env_id", -readonly=>"readonly")]);
print "</tr>\n";
print "<tr><td colspan=2>\n";
print "<table>\n";
print "<tr>\n";
print "<td>\n";
print "\n<input type=\"button\" onClick='clearForm(\"R\");' value=\"Clear Fields\" /></input>\n";
print "</td>\n";
print "<td>\n";
#print "&nbsp;";
print $q->submit(-value => "Search", -name => "r_search");
#print "<br>";
print "</tr><tr>\n";
print "<td colspan=2>\n";
print "Find items in CMDB_COMP_USES/CMDB_INCLUDES which use/include this Env ID.<br>\n";
print "</td></tr><tr>\n";
print "<td>\n";
print $q->submit(-value => "Find Users", -name => "inputtype");
print "</td>\n";
print "<td>\n";
#print "&nbsp;";
print $q->submit(-value => "Find Includers", -name => "inputtype");
print "</td></tr></table>\n";
print "</td></tr>\n";
print "</table>\n";
print "</td></tr>\n";



#print "<tr><td colspan=2>\n";
print "<tr>\n";
if($inputtype eq "Current Relationship") {
	$l_name = $q->param('l_name');
	$r_name = $q->param('r_name');
	$l_env_id = $q->param('l_env_id');
	$r_env_id = $q->param('r_env_id');
	#
	# Kinda weird.  Check for a relationship (or
	# non-relationship, as in the first case), and
	# if that relationship isn't true, then, in the
	# else, select from the database to provide the
	# data for the next if/else to check on that
	# condition.
	#
	if($l_env_id == 0 || $l_env_id eq "" || $r_env_id == 0 || $r_env_id eq "") {
		$ans = "<td align='center'>&nbsp;</td><td align='center'>Er, one or both env IDs is blank.</td><td align='center'>&nbsp;</td>";
		$k = -1;
	} else {
		$getTbl = $dbh->prepare("select * from cmdb_comp_uses where comp_env_id_user=$l_env_id and comp_env_id_used=$r_env_id") or die "Error preparing statement: $DBI::errstr\n";
		$getTbl->execute();
		$k = 0;
		while(($i, $j) = $getTbl->fetchrow()) {
			$k++;
		}
	}
	if($k > 0) {
		$ans = "<td align='center'>$l_name (env id = $l_env_id)</td><td align='center'>uses</td><td align='center'>$r_name (env id = $r_env_id)</td>";
		$k = -1;
	} elsif($k != -1) {
		$getTbl = $dbh->prepare("select * from cmdb_comp_uses where comp_env_id_user=$r_env_id and comp_env_id_used=$l_env_id") or die "Error preparing statement: $DBI::errstr\n";
		$getTbl->execute();
		$k = 0;
		while(($i, $j) = $getTbl->fetchrow()) {
			$k++;
		}
	}
	if($k > 0) {
		$ans = "<td align='center'>$l_name (env id = $l_env_id)</td><td align='center'>used by</td><td align='center'>$r_name (env id = $r_env_id)</td>";
		$k = -1;
	} elsif($k != -1) {
		$getTbl = $dbh->prepare("select * from cmdb_includes where comp_env_id_includer=$l_env_id and comp_env_id_included=$r_env_id") or die "Error preparing statement: $DBI::errstr\n";
		$getTbl->execute();
		$k = 0;
		while(($i, $j) = $getTbl->fetchrow()) {
			$k++;
		}
	}
	if($k > 0) {
		$ans = "<td align='center'>$l_name (env id = $l_env_id)</td><td align='center'>includes</td><td align='center'>$r_name (env id = $r_env_id)</td>";
		$k = -1;
	} elsif($k != -1) {
		$getTbl = $dbh->prepare("select * from cmdb_includes where comp_env_id_includer=$r_env_id and comp_env_id_included=$l_env_id") or die "Error preparing statement: $DBI::errstr\n";
		$getTbl->execute();
		$k = 0;
		while(($i, $j) = $getTbl->fetchrow()) {
			$k++;
		}
	}
	if($k > 0) {
		$ans = "<td align='center'>$l_name (env id = $l_env_id)</td><td align='center'>is included by</td><td align='center'>$r_name (env id = $r_env_id)</td>";
		$k = -1;
	}
	if($k >= 0) {
		$ans = "<td align='center'>&nbsp;</td><td align='center'>No Relationship.</td><td align='center'>&nbsp;</td>";
		#$ans = "No Relationship.";
	}
	print $ans;
}
#print "</td></tr>\n";
print "</tr>\n";


print "</table>\n";
print "<br>\n";
print $q->submit(-value => "Create New", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Current Relationship", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

#
# Since we need to recognize which search was
# clicked, there are two possible submit string
# names for the search: l_search and r_search.  If 
# either one of those was defined, then we need
# to do a search.
#
if(defined($q->param('l_search')) || defined($q->param('r_search'))) {
	my $name;
	my $type;
	my $system_id;
	my $env;
	my $server;
	my $server_id;
	my $app_server;
	my $deploy_location;
	my $a = 0;
	if(defined($q->param('l_search'))) {
		$name = $q->param('l_name');
		$type = $q->param('l_type');
		$system_id = $q->param('l_system_id');
		$env = $q->param('l_env');
		$server = $q->param('l_server');
		$server_id = $q->param('l_server_id');
		$deploy_location = $q->param('l_deploy_location');
		$app_server = $q->param('l_app_server');
		$side = "L";
		print "<u>Clicking on th Env ID will populate left side fields.</u><br>\n";
	} else {
		$name = $q->param('r_name');
		$type = $q->param('r_type');
		$system_id = $q->param('r_system_id');
		$env = $q->param('r_env');
		$server = $q->param('r_server');
		$server_id = $q->param('r_server_id');
		$deploy_location = $q->param('r_deploy_location');
		$app_server = $q->param('r_app_server');
		$side = "R";
		print "<u>Clicking on th Env ID will populate right side fields.</u><br>\n";
	}
	#
	# Build search string
	#
	if(length($name) > 0) {
		#
		# Replace any blanks with a wildcard...
		#
		$name =~ s/ /%/g;
		$search_string = "lower(comp_name) like lower('%$name%')";
		$a = 1;
	}
	if(length($system_id) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(system_id) like lower('%$system_id%')";
		} else {
			$search_string = "lower(system_id) like lower('%$system_id%')";
			$a = 1;
		}
	}
	if(length($deploy_location) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(deploy_location) like lower('%$deploy_location%')";
		} else {
			$search_string = "lower(deploy_location) like lower('%$deploy_location%')";
			$a = 1;
		}
	}
	if(length($app_server) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(app_server) like lower('%$app_server%')";
		} else {
			$search_string = "lower(app_server) like lower('%$app_server%')";
			$a = 1;
		}
	}
	if(length($type) > 0) {
		if($a) {
			$search_string = $search_string . " and comp_type='$type'";
		} else {
			$search_string = "comp_type='$type'";
			$a = 1;
		}
	}
	if(length($env) > 0) {
		if($a) {
			$search_string = $search_string . " and env='$env'";
		} else {
			$search_string = "env='$env'";
			$a = 1;
		}
	}
	#if($server_id > 0) {
	if($server > 0) {
		if($a) {
			#$search_string = $search_string . " and server_id=$server_id";
			$search_string = $search_string . " and server_id=$server";
		} else {
			#$search_string = "server_id=$server_id";
			$search_string = "server_id=$server";
			$a = 1;
		}
	}
	if($a != 0) {
		$search_string = "where " . $search_string . " and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
	} else {
		$search_string = "where cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
	}
        $getTbl = $dbh->prepare("select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env $search_string order by comp_name") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Env ID</th>\n";
	print "<th>Comp Name</th>\n";
	print "<th>Comp Type</th>\n";
	print "<th>System ID</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server ID</th>\n";
	print "<th>App Server</th>\n";
	print "<th>Deploy Location</th>\n";
	print "</tr>\n";
	while (($comp_id, $comp_env_id, $comp_name, $comp_type, $system_id, $env, $server_id, $app_server, $deploy_location) = $getTbl->fetchrow()) {
		if(length($comp_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm('$side', $comp_id, $comp_env_id, '$comp_name', '$comp_type', '$system_id', '$env', $server_id, '$app_server', '$deploy_location')\">$comp_env_id</a></td>\n";
			print "<td>$comp_name</td>\n";
			if(length($comp_type) < 1) { $comp_type = "&nbsp;"; }
			print "<td>$comp_type</td>\n";
			if(length($system_id) < 1) { $system_id = "&nbsp;"; }
			print "<td>$system_id</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($app_server) < 1) { $app_server = "&nbsp;"; }
			print "<td>$app_server</td>\n";
			if(length($deploy_location) < 1) { $deploy_location = "&nbsp;"; }
			print "<td>$deploy_location</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
	

} elsif (($inputtype eq 'Find Users') || ($inputtype eq 'Find Used By') || ($inputtype eq 'Find Includers') || ($inputtype eq 'Find Included By')) {
	my $name;
	my $verb;
	my $env_id;
	my $side;
	if($inputtype eq 'Find Used By') {
		$name = $q->param('l_name');
		$verb = "uses";
		$env_id = $q->param('l_env_id');
		$insstr = "select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env, cmdb_comp_uses where comp_env_id_user=$env_id and comp_env_id_used=comp_env_id and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
		$side = "R";
	} elsif ($inputtype eq 'Find Users') {
		$name = $q->param('r_name');
		$verb = "used by";
		$env_id = $q->param('r_env_id');
		$insstr = "select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env, cmdb_comp_uses where comp_env_id_used=$env_id and comp_env_id_user=comp_env_id and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
		$side = "L";
	} elsif ($inputtype eq 'Find Included By') {
		$name = $q->param('l_name');
		$verb = "includes";
		$env_id = $q->param('l_env_id');
		$insstr = "select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env, cmdb_includes where comp_env_id_includer=$env_id and comp_env_id_included=comp_env_id and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
		$side = "R";
	} elsif ($inputtype eq 'Find Includers') {
		$name = $q->param('r_name');
		$verb = "included by";
		$env_id = $q->param('r_env_id');
		$insstr = "select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env, cmdb_includes where comp_env_id_included=$env_id and comp_env_id_includer=comp_env_id and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
		$side = "L";
	} 
	if($env_id == 0 || $env_id eq "") {
		print "<p><b>No Environment ID specified.  Can't continue.</b></p>\n";
		print $q->end_html();
		#$dbh->commit();
		$dbh->disconnect;
		exit;
	}
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
	$getTbl->execute();
	printf "<p>Clicking on the environment ID will populate the %s side of the table.", ($side eq "R")?"right":"left";
	print "<p>$name $verb:";
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Env ID</th>\n";
	print "<th>Comp Name</th>\n";
	print "<th>Comp Type</th>\n";
	print "<th>System ID</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server ID</th>\n";
	print "<th>App Server</th>\n";
	print "</tr>\n";
	while (($comp_id, $comp_env_id, $comp_name, $comp_type, $system_id, $env, $server_id, $app_server, $deploy_location) = $getTbl->fetchrow()) {
		if(length($comp_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm('$side', $comp_id, $comp_env_id, '$comp_name', '$comp_type', '$system_id', '$env', $server_id, '$app_server', '$deploy_location')\">$comp_env_id</a></td>\n";
			print "<td>$comp_name</td>\n";
			if(length($comp_type) < 1) { $comp_type = "&nbsp;"; }
			print "<td>$comp_type</td>\n";
			if(length($system_id) < 1) { $system_id = "&nbsp;"; }
			print "<td>$system_id</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($app_server) < 1) { $app_server = "&nbsp;"; }
			print "<td>$app_server</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	my $l_env = $q->param('l_env_id');
	my $r_env = $q->param('r_env_id');
	my $uses = $q->param('uses');
	my $l_type = $q->param('l_type');
	my $r_type = $q->param('r_type');
	my $er;
	my $ed;
	if($uses eq "uses") {
		$table = "cmdb_comp_uses";
		$er = $l_env;
		$ed = $r_env;
		$fields = "(comp_env_id_user, comp_env_id_used)";
	} elsif ($uses eq "used by") {
		$table = "cmdb_comp_uses";
		$er = $r_env;
		$ed = $l_env;
		$fields = "(comp_env_id_user, comp_env_id_used)";
	} elsif ($uses eq "includes") {
		if($l_type ne "DomainArchive") {
			print "<p><b>Error: An Includer must have a type of \"DomainArchive\".</b></p>\n";
			print $q->end_html();
			#$dbh->commit();
			$dbh->disconnect;
			exit;
		}
		$table = "cmdb_includes";
		$er = $l_env;
		$ed = $r_env;
		$fields = "(comp_env_id_includer, comp_env_id_included)";
	} elsif ($uses eq "included by") {
		if($r_type ne "DomainArchive") {
			print "<p><b>Error: An Includer must have a type of \"DomainArchive\".</b></p>\n";
			print $q->end_html();
			#$dbh->commit();
			$dbh->disconnect;
			exit;
		}
		$table = "cmdb_includes";
		$er = $r_env;
		$ed = $l_env;
		$fields = "(comp_env_id_includer, comp_env_id_included)";
	}

#print "<br>er=::$er::, ed=::$ed::<br>\n";
	if($er == 0 || $er eq "" || $ed == 0 || $ed eq "") {
		if($er == 0 || $er eq "") {
			$side = "USER";
		} else {
			$side = "USED";
		}
		print "<p><b>Missing Environment ID for $side.  Can't continue.</b></p>\n";
		print $q->end_html();
		#$dbh->commit();
		$dbh->disconnect;
		exit;
	}

	$insstr = "insert into $table $fields values ($er, $ed)";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_COMP_USES completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_COMP_USES failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
	my $l_env = $q->param('l_env_id');
	my $r_env = $q->param('r_env_id');
	my $uses = $q->param('uses');
	my $l_type = $q->param('l_type');
	my $r_type = $q->param('r_type');
	my $er;
	my $ed;

	if($uses eq "uses") {
		$table = "cmdb_comp_uses";
		$er = $l_env;
		$ed = $r_env;
		$l_fld = "comp_env_id_user";
		$r_fld = "comp_env_id_used";
	} elsif ($uses eq "used by") {
		$table = "cmdb_comp_uses";
		$er = $r_env;
		$ed = $l_env;
		$l_fld = "comp_env_id_user";
		$r_fld = "comp_env_id_used";
	} elsif ($uses eq "includes") {
		$table = "cmdb_includes";
		$er = $l_env;
		$ed = $r_env;
		$l_fld = "comp_env_id_includer";
		$r_fld = "comp_env_id_included";
	} elsif ($uses eq "included by") {
		$table = "cmdb_includes";
		$er = $r_env;
		$ed = $l_env;
		$l_fld = "comp_env_id_includer";
		$r_fld = "comp_env_id_included";
	}

	if($er < 1 || $er eq "" | $ed < 1 || $ed eq "") {
                print"<p><b>A valid ENV ID must exist in both tables.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

	if($er == $ed) {
                print"<p><b>User ENV ID must be different from Used ENV ID.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "delete from $table where $l_fld=$er and $r_fld=$ed";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_COMP_USES completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_COMP_USES failed:</p>\n";
                print "<p>$insstr</p>\n";
        }

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


