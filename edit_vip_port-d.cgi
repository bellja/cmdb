#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Create out headers and the Javascript functions
# we'll need.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
print ".fixed {font-size:12px}\n";
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(vp_id, vip, port, server_id, server_name) {
	document.editFields.vp_id.value = vp_id;
	document.editFields.vip.value = vip;
	document.editFields.port.value = port;
	document.editFields.server[0].selected = true;
	for(i = 0; i < document.editFields.server.length; i++) {
		//if (document.editFields.server[i].value == server_name) {
		if (document.editFields.server[i].value == server_id) {
			document.editFields.server[i].selected = true;
		}
	}

}
function clearForm() {
	document.editFields.vp_id.value = '';
	document.editFields.vip.value = '';
	document.editFields.port.value = '';
	document.editFields.server[0].selected = true;
}
function confirmDelete() {
        var answer = confirm(\"Delete this vip port record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}
\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_VIP_PORT');

print "<b>Edit Vip Port Table<b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing
statement: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_os[0]->{server_name} = "";
$a_os[0]->{server_id} = 0;
$a_os[0]->{env} = "";
while (($a_os[$i]->{server_name}, $a_os[$i]->{server_id}, $a_os[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

if($q->param('server') eq "") {
	$cur_server = 0;
} else {
	$cur_server = $q->param('server');
}

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";

print "<table>\n";
print "<tr>\n";
print $q->td(["VIP:", $q->textfield(-name=>"vip", -size=>50)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Port:", $q->textfield(-name=>"port")]);
print "</tr>\n";
print "<tr><td>\n";
print "Server:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='server' class=fixed>\n";
#print "<select name='server_id'>\n";
$s = 0;
for($i = 0; $i <= $#a_os; $i++) {
	print "<option value=\"$a_os[$i]->{server_id}\"";
	if($cur_server == $a_os[$i]->{server_id} && $s == 0) {
		print " selected";
		$s = 1;
	}
	print ">\n";
	if($i > 0 && length($a_os[$i]->{server_name}) > 0) {
		printf "%10.10s - %4.4s - %6.6s", $a_os[$i]->{server_name}, $a_os[$i]->{server_id}, $a_os[$i]->{env};
	} else {
		print "&nbsp;"
	}
	print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["VP_ID:", $q->textfield(-name=>"vp_id", -readonly=>"readonly")]);
print "</tr>\n";
print "</table>\n";
print "\n<br>\n";
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";

#
# If $q->param() equals "", then it's either the
# first time through, or something similar.
#
if ($inputtype eq 'Search') {
	my $vip = $q->param('vip');
	my $port = $q->param('port');
	my $server_id = $q->param('server');
	my $a = 0;
	#
	# Build while string
	#
	# Tell oracle to translate to all lower
	# case so that we're doing a case insensitive
	# search.
	#
	if(length($vip) > 0) {
		$search_string = "lower(vip) like lower('%$vip%')";
		$a = 1;
	}
	if(length($port) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(port) like lower('%$port%')";
		} else {
			$search_string = "lower(port) like lower('%$port%')";
			$a = 1;
		}
	}
	if($server_id > 0) {
		if($a) {
			$search_string = $search_string . " and server_id=$server_id";
		} else {
			$search_string = "server_id=$server_id";
			$a = 1;
		}
	}
	if($a) {
		$insstr = "select * from cmdb_vip_port where $search_string order by vip";
		$getTbl = $dbh->prepare("select * from cmdb_vip_port where $search_string order by vip") or die "Error preparing statement: $DBI::errstr\n";
	} else {
		$insstr = "select * from cmdb_vip_port order by vip";
		$getTbl = $dbh->prepare("select * from cmdb_vip_port order by vip") or die "Error preparing statement: $DBI::errstr\n";
	}
#print "<p>$insstr</p>\n";
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>VP_ID</th>\n";
	print "<th>VIP</th>\n";
	print "<th>Port</th>\n";
	print "<th>Server ID</th>\n";
	print "</tr>\n";
	while (($vp_id, $vip, $port, $server_id) = $getTbl->fetchrow()) {
		if(length($vip) > 0) {
			print "<tr>\n";
			for($i = 0; $i <= $#a_os; $i++) {
#print "<br>$server_id vs $a_os[$i]->{server_id}\n";
				if($server_id == $a_os[$i]->{server_id}) {
					$server_name = "$a_os[$i]->{server_name} - $a_os[$i]->{server_id} - $a_os[$i]->{env}";
					last;
				}
			}
			print "<td><a href=\"#\" onClick=\"fillForm($vp_id, '$vip', '$port', '$server_id', '$server_name')\">$vp_id</a></td>\n";
			print "<td>$vip</td>\n";
			print "<td>$port</td>\n";
			print "<td>$server_id</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
	

} elsif ($inputtype eq 'Create New' || $inputtype eq "Continue Submit") {
	my $vip = $q->param('vip');
	my $port = $q->param('port');
	my $server_id = $q->param('server');
	if(length($vip) < 1 || length($port) < 1 || $server_id == 0) {
		print "<p><b>Error: All fields must have values</b></p>\n";
		print $q->end_form();
		print $q->end_html();
		#$dbh->commit();
		$dbh->disconnect;
		exit;
	}
	if($server_id != 0 && $server_id ne "" && $inputtype eq "Create New") {
		$insstr = "select env from cmdb_server where server_id=$server_id";
		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		$temp = $getTbl->fetchrow();
		$getTbl->finish;
		my $a = lc($vip);
		my $b = lc($temp);
		if($a !~ /$b/) {
			print "<p><b>The environment contained within the VIP ($vip) doesn't\n";
			print "match the environment of the server ($temp).  Please\n";
			print "confirm this update.</b></p>\n";
			print $q->submit(-value => "Continue Submit", -name => "inputtype");
			print $q->end_form();
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}
	}

	$insstr = "select vp_id from cmdb_vip_port where server_id=$server_id and lower(vip) = lower('$vip') and port='$port'";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while ($vp_id = $getTbl->fetchrow()) {
		$i++
	}
	$getTbl->finish;
	if($i > 0) {
		print "<p><b>Duplicate row detected.  No changes made to table.</b></p>\n";
		print $q->end_form();
		print $q->end_html();
		#$dbh->commit();
		$dbh->disconnect;
		exit;
	}

	$insstr = "insert into cmdb_vip_port (vip, port, server_id) values ('$vip', '$port', '$server_id')";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_VIP_PORT completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_VIP_PORT failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Update' || $inputtype eq "Continue Update") {
	my $vip = $q->param('vip');
	my $port = $q->param('port');
	my $server_id = $q->param('server');
	my $vp_id = $q->param('vp_id');
	if($vp_id < 1) {
		print"<p><b>VP_ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
	}
	if($server_id != 0 && $server_id ne "" && $inputtype eq "Update") {
		$insstr = "select env from cmdb_server where server_id=$server_id";
		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		$temp = $getTbl->fetchrow();
		$getTbl->finish;
		my $a = lc($vip);
		my $b = lc($temp);
		if($a !~ /$b/) {
			print "<p><b>The environment contained within the VIP ($vip) doesn't\n";
			print "match the environment of the server ($temp).  Please\n";
			print "confirm this update.</b></p>\n";
			print $q->submit(-value => "Continue Update", -name => "inputtype");
			print $q->end_form();
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}
	}
	$insstr = "update cmdb_vip_port set vip='$vip', port='$port', server_id='$server_id' where vp_id=$vp_id";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Update to CMDB_VIP_PORT completed.</p>\n";
	} else {
		print "<p>Update to CMDB_VIP_PORT failed:</p>\n";
		print "<p>$insstr</p>\n";
	}
} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $vp_id = $q->param('vp_id');
        if($vp_id < 1 || $vp_id == "") {
                print"<p><b>VP ID is zero.  Can't delete that.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

        $insstr = "select vp_id from cmdb_comp_env where vp_id=$vp_id";
        #print "<br>$insstr<br>\n";
        $getTbl = $dbh->prepare($insstr);
        $getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
                $i++;
        }
        $getTbl->finish;
        if($i != 0) {
                print "<p><b>This VP ID is still in use in the CMDB_COMP_ENV table.  No changes made to CMDB_VIP_PORT table.</b></p>\n";
		print $q->end_form();
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }


        $insstr = "delete from cmdb_vip_port where vp_id=$vp_id";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_VIP_PORT completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_VIP_PORT failed:</p>\n";
                print "<p>$insstr</p>\n";
        }


}
print $q->end_form();
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


