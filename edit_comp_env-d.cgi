#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# This script is a bit different than then other
# CMDB edit scripts.  The others deal only with a
# single table.  This script deals with two, while
# only updating one.  It allows the user to search
# using fields from the cmdb_comp_gen table and
# get results from the cmdb_comp_gen, cmdb_comp_env,
# and the cmdb_language table, though the latter is
# used for only a single field.
#
# Add to that the problem that we've got seven
# dropdown lists to populate at run time, and set
# when the user selects something in the search
# results, and it makes for a more complicated
# script than is usual.
#

# This is used when a select option has no value
# when retrieved from the database.  It's nicer
# then simply leaving the select empty. Problem
# is, it then becomes a value, and we need to
# know what that value is.  To prevent having a
# "magic" string that appears various places in
# the code, including in a compare, set one here
# we can use everywhere.
#
$NONESTR = "(none)";
#
# Get a list of langauges and versions
# We need to do this now, because we have to
# set some stuff up in a Javascript function.
#
$getTbl = $dbh->prepare("select unique language, version, comp_env_id, lang_id from cmdb_language order by language, version") or die "Error preparing
statement: $DBI::errstr\n";
$getTbl->execute();
$langdata[0] = "";
$langnames[0] = "";
$lang[0]->{language} = "";
$lang[0]->{version} = "";
$lang[0]->{land_id} = 0;
$lang[0]->{comp_env_id} = 0;
$i = 1;
$j = 0;
$k = 0;
while (($lang[$i]->{language}, $lang[$i]->{version}, $lang[$i]->{comp_env_id},  $lang[$i]->{lang_id}) = $getTbl->fetchrow()) {
        if(($lang[$i]->{language} ne $lang[$i - 1]->{language}) || ($lang[$i]->{version} ne $lang[$i - 1]->{version})) {
		if($lang[$i]->{language} ne $lang[$i - 1]->{language}) {
			$langnames[$k] = $lang[$i]->{language};
			$k++;
		}
                $langdata[$j]->{language} = $lang[$i]->{language};
                $langdata[$j]->{version} = $lang[$i]->{version};
                $langdata[$j]->{lang_id} = $lang[$i]->{lang_id};
                $langdata[$j]->{comp_env_id} = $lang[$i]->{lang_id};
                $j++;
        }
        $i++;
}
$getTbl->finish;

#
# Get a list of VIP IDs and names (VIPs)
# Again, we need this for a JS function.
#
$getTbl = $dbh->prepare("select vip, vp_id, server_id, port from cmdb_vip_port order by vip, vp_id") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$vip[0]->{vip} = "";
$vip[0]->{vp_id} = 0;
$vip[0]->{server_id} = 0;
$vip[0]->{port} = 0;
$i = 1;
while (($vip[$i]->{vip}, $vip[$i]->{vp_id}, $vip[$i]->{server_id}, $vip[$i]->{port}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$server[0]->{server_name} = "";
$server[0]->{server_id} = 0;
$server[0]->{env} = "";
$i = 1;
while (($server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Create out headers and the Javascript functions
# we'll need.
#
# fillForm() populates both sets of fields with data
# returned from a search.  The loops simply run through
# current data and mark the correct field as selected.
# Same idea for the checkboxes.
#
# clearForm() and clearGenForm() set all values to null,
# all select lists to 0 as the selected element, and 'Y'
# to the checkboxes.  Might make sense to clear both forms
# with the same keypress, but that doesn't happen right now.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
print ".fixed {font-size:12px}\n";
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function confirmDelete() {
	var answer = confirm(\"If the current COMP_ENV_ID exists in one or more of the CMDB_COMP_USES, CMDB_INCLUDES, CMDB_LANGUAGES, or CMDB_DB_USES tables, the entries will be removed from those tables as well.  Delete this environment record?\");
	if (answer){
		document.editFields.inputtype.value = \"Delete\";
		document.editFields.DELETE.value = \"yes\";
		document.editFields.submit();
	}
}

function fillForm(comp_env_id, gen_comp_id, comp_name, gen_comp_type, system_id, env, app_server, clustered, high_avail, deploy_location, comp_type, comp_id, server_id, vp_id, state, language, lang_id) {
	var i;
	clearForm();
	clearGenForm();
	document.editFields.gen_name.value = comp_name;
	document.editFields.gen_type.value = gen_comp_type;
	document.editFields.gen_system_id.value = system_id;
	document.editFields.gen_comp_id.value = gen_comp_id;
	document.editFields.gen_type[0].selected = true;
	document.editFields.comp.value = comp_id;
	document.editFields.env.value = env;
	document.editFields.app_server.value = app_server;
	document.editFields.language_id.value = lang_id;
	if(clustered == 'Y') {
		document.editFields.clustered[0].checked = true;
		document.editFields.clustered[1].checked = false;
	} else {
		document.editFields.clustered[0].checked = false;
		document.editFields.clustered[1].checked = true;
	}
	if(high_avail == 'Y') {
		document.editFields.high_avail[0].checked = true;
		document.editFields.high_avail[1].checked = false;
	} else {
		document.editFields.high_avail[0].checked = false;
		document.editFields.high_avail[1].checked = true;
	}
	document.editFields.deploy_location.value = deploy_location;
	document.editFields.gen_type[0].selected = true;
	for(i = 0; i < document.editFields.gen_type.length; i++) {
		if (document.editFields.gen_type[i].value == gen_comp_type) {
			document.editFields.gen_type[i].selected = true;
		}
	}
	document.editFields.env[0].selected = true;
	document.editFields.gen_env[0].selected = true;
	for(i = 0; i < document.editFields.env.length; i++) {
		if (document.editFields.env[i].value == env) {
			document.editFields.env[i].selected = true;
			document.editFields.gen_env[i].selected = true;
		}
	}
	document.editFields.server[0].selected = true;
	for(i = 0; i < document.editFields.server.length; i++) {
		if (document.editFields.server[i].value == server_id) {
			document.editFields.server[i].selected = true;
		}
	}
	serverVip();
	// document.editFields.vip[0].selected = true;
	for(i = 0; i < document.editFields.vip.length; i++) {
		if (document.editFields.vip[i].value == vp_id) {
			document.editFields.vip[i].selected = true;
		}
	}
	document.editFields.state[0].selected = true;
	for(i = 0; i < document.editFields.state.length; i++) {
		if (document.editFields.state[i].value == state) {
			document.editFields.state[i].selected = true;
		}
	}
	document.editFields.lang[0].selected = true;
	for(i = 0; i < document.editFields.lang.length; i++) {
		if (document.editFields.lang[i].value == language) {
			document.editFields.lang[i].selected = true;
		}
	}
	langVersion(comp_env_id);
	// langVersion(lang_id);
	document.editFields.comp_env_id.value = comp_env_id;

	
}
function clearForm() {
	document.editFields.comp.value = '';
	document.editFields.env.value = '';
	document.editFields.version.value = '';
	document.editFields.server[0].selected = true;
	document.editFields.app_server.value = '';
	document.editFields.as_version.value = '';
	document.editFields.comp_env_id.value = '';
	document.editFields.clustered[0].checked = true;
	document.editFields.clustered[1].checked = false;
	document.editFields.high_avail[0].checked = true;
	document.editFields.high_avail[1].checked = false;
	document.editFields.deploy_location.value = '';
	document.editFields.vip[0].selected = true;
	document.editFields.state[0].selected = true;
	document.editFields.lang[0].selected = true;
	removeAllOptions(document.editFields.langversion);
	document.editFields.langversion[0] = new Option('Select Version', '', false, true);
	document.editFields.langversion[0].selected = true;
}
function clearGenForm() {
	document.editFields.gen_name.value = '';
	document.editFields.gen_type[0].selected = true;
	document.editFields.gen_system_id.value = '';
	document.editFields.gen_comp_id.value = '';
	document.editFields.gen_env[0].selected = true;
}
function removeAllOptions(selectbox) {
	var i;
	for(i = selectbox.options.length - 1; i >= 0; i--) {
		selectbox.remove(i);
	}
}

\n";
#
# I know, this is ugly and confusing to read.  Sorry, but I
# don't really know of a better way to do what needs to be
# done.  Your best bet is to first take a look at the output,
# looking at the langVersion() function to see what was produced,
# then you can more easily figure out what's going on here as
# I build the function a piece at a time.
#
# Basically, this creates a function that will populate the
# language version select box with the possible language versions
# for the selected language.
#
print "function langVersion(env_id) {\n";
print "\tvar tf;\n";
#print "alert(env_id);\n";
print "\tremoveAllOptions(document.editFields.langversion);\n";
print "\tif(env_id == 0) {\n";
print "\t\tdocument.editFields.langversion[0] = new Option('Select Version', '', false, true);\n";
#print "\t\tenv_id++;\n";
print "\t} else {\n";
print "\t\tdocument.editFields.langversion[0] = new Option('Select Version', '', false, false);\n";
print "\t}\n";
for($i = 0; $i <= $#langnames; $i++) {
	print "\tif(document.editFields.lang.value == \"$langnames[$i]\") {\n";
	$k = 0;
	for($j = 0; $j <= $#langdata; $j++) {
		if($langnames[$i] eq $langdata[$j]->{language}) {
			$t_version = $langdata[$j]->{version};
			if(length($t_version) < 1) {
				$t_version = $NONESTR;
			}
			print "\t\tif(env_id == $langdata[$j]->{comp_env_id}) {\n";
			print "\t\t\ttf = true;\n";
			print "\t\t} else {\n";
			print "\t\t\ttf = false;\n";
			print "\t\t}\n";
#			if($t_version ne $langdata[$j - 1]->{version}) {
				print "\t\tdocument.editFields.langversion[$k] = new Option('$t_version', '$langdata[$j]->{lang_id}', false, tf);\n";
				$k++;
#			}
		}
	}
	print "\t}\n";
}
print "}\n";

#
# Again, your best bet is to read the javascript
# output before trying to read this, but here's essentially
# what it does:
#
# This function matches VIP IDs with Server IDs.  The idea
# is that we don't want a user to select a server, then select
# a vip not associated witht that server.  Above we've already
# read the vip table and created an array that allows us to
# associate vip names and vip IDs with a specific server.
#
# The JS function contains essentially two parts. First is the
# creation of the the arrays.  Looking at the JS, it looks like
# a lot of typing, but Perl creates each one of those lines.
# Next is the loop that recreates the values within the vip
# select statement.
#
print "function serverVip() {\n";
#print "\tvar s_name = [];\n";
#print "\tvar s_id = [];\n";
print "\tvar v_name = [];\n";
print "\tvar v_id = [];\n";
print "\tvar v_server_id = [];\n";
print "\tvar i, k;\n";
for($i = 1; $i < $#vip; $i++) {
	print "\tv_name[$i]='$vip[$i]->{vip} / $vip[$i]->{port}'; v_id[$i]=$vip[$i]->{vp_id}; v_server_id[$i]=$vip[$i]->{server_id};\n";
}
print "\tlast_vip = $i;\n";
print "\tremoveAllOptions(document.editFields.vip);\n";
print "\tdocument.editFields.vip[0] = new Option('', 0, false, true);\n";
print "\tk = 1;\n";
print "\tfor(i = 0; i < last_vip; i++) {\n";
print "\t\tif(document.editFields.server.value == v_server_id[i]) {\n";
print "\t\t\tdocument.editFields.vip[k] = new Option(v_name[i], v_id[i], false, false);\n";
print "\t\t\tk++;\n";
print "\t\t}\n";
print "\t}\n";
print "}\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_COMP_ENV');

print "<b>Edit Component Environment Table</b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');

#
# First get info to fill our dropdown selects
#

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$server[0]->{server_name} = "";
$server[0]->{server_id} = 0;
$server[0]->{env} = "";
$i = 1;
while (($server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Get a list of comp_ids and comp_names
#
$getTbl = $dbh->prepare("select comp_name, comp_id from cmdb_comp_gen order by comp_name, comp_id") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$comp[0]->{comp_name} = "";
$comp[0]->{comp_id} = 0;
$i = 1;
while (($comp[$i]->{comp_name}, $comp[$i]->{comp_id}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

if($q->param('server') eq "") {
	$cur_server = 0;
} else {
	$cur_server = $q->param('server');
}
if($q->param('vip') eq "") {
	$cur_vip = 0;
} else {
	$cur_vip = $q->param('vip');
}
if($q->param('comp') eq "") {
	$cur_comp = 0;
} else {
	$cur_comp = $q->param('comp');
}
if($q->param('lang') eq "") {
	$cur_lang = 0;
} else {
	$cur_lang = $q->param('lang');
}

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statemen
t: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";

#
# Okay, we're putting up the comp_gen form, but
# only to allow searches.  The searchs, when selected
# will add data to the comp_env form below this one.
# The user will not be able to edit the comp_gen data
# from here.
#
print "<p>\n";
print "Enter information into the Compenent Gen Table, then click on 'Search'.\n";
print "Clicking on the component environment ID in the search results will\n";
print "fill both tables with the available data.\n";
print "</p>\n";
print "<p>\n";
print "<b>Note:</b> Before adding a new entry to the CMDB_COMP_ENV table,\n";
print "be sure to have populated the CMDB_SERVER, CMDB_LANGUAGE, and\n";
print "CMDB_VIP_PORT tables with the appropriate information.\n";
print "</p>\n";
 
#
#
print "<h><u>Search Fields</u></h>\n";
#
#
 
print "<table>\n";
print "<tr>\n";
print "<td colspan=2>\n";
print "<h><u>Component Gen Table Search Fields</u></h>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Name:", $q->textfield(-name=>"gen_name", -size=>50)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Type:", $q->popup_menu(-name=>"gen_type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["System ID:", $q->textfield(-name=>"gen_system_id")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Comp ID:", $q->textfield(-name=>"gen_comp_id", -readonly=>"readonly")]);
print "</tr>\n";
print "<tr>\n";
print "<td colspan=2>\n";
print "<h><u>Component Environment Table Search Fields</u></h>\n";
print "</td></tr>\n";
print "<tr>\n";
print "<td>Environment</td>\n";
print "<td>\n";
print $q->popup_menu(-name=>"gen_env", -values=>\@a_env);
print "</td>\n";
print "</tr>\n";
print "</table>\n";

=pod
print $q->table(
        $q->Tr(
        [
                $q->td(["Name:", $q->textfield(-name=>"gen_name", -size=>50)]),
                $q->td(["Type:", $q->popup_menu(-name=>"gen_type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]),
                $q->td(["System ID:", $q->textfield(-name=>"gen_system_id")]),
                $q->td(["Comp ID:", $q->textfield(-name=>"gen_comp_id", -readonly=>"readonly")])
        ])
);
=cut
print "<br>\n";
print "\n<input type=\"button\" onClick=\"clearGenForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "\n<br><br>\n";

#
#
print "<h><u>Component Environment Table</u></h>\n";
#
#

print "<table width=100%>\n";
#print "<tr><td>\n";
#print "Comp ID:\n";
#print "</td>\n";
#print "</td><td width=15%>\n";
#print "<select name='comp' class=fixed>\n";
#$s = 0;
#for($i = 0; $i <= $#comp; $i++) {
#	if($i > 0 && length($comp[$i]->{comp_name}) < 1) {
#		next;
#	}
#	print "<option value=\"$comp[$i]->{comp_id}\"";
#	if($cur_comp == $comp[$i]->{comp_id} && $s == 0) {
#		print " selected";
#		$s = 1;
#	}
#	print ">\n";
#	if($i > 0 && length($comp[$i]->{comp_name}) > 0) {
#		printf "%s", $comp[$i]->{comp_name};
#	} else {
#		print "&nbsp;"
#	}
#	print "</option>\n";
#}
#print "</select>\n";
#print "</td></tr>\n";
#print "</tr>\n";
#print "<tr>\n";
#print $q->td({width=>"10%"}, ["Comp ID", $q->textfield(-name=>"comp", -readonly=>"readonly"))]);
#print "</tr>\n";
print "\n<input type=\"hidden\" name=\"comp\" />\n";
print "<tr><td width=\"10%\">\n";
print "Environment:\n";
print "</td><td width=\"10%\">\n";
#print $q->popup_menu(-name=>"env", -values=>["", "PROD","SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"]);
print $q->popup_menu(-name=>"env", -values=>\@a_env);
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Version:", $q->textfield(-name=>"version")]);
print "</tr>\n";
print "<tr><td>\n";
print "Server:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='server' class=fixed class=fixed onChange=\"serverVip();\">\n";
#print "<select name='server_id'>\n";
$s = 0;
for($i = 0; $i <= $#server; $i++) {
	print "<option value=\"$server[$i]->{server_id}\"";
	if($cur_server == $server[$i]->{server_id} && $s == 0) {
		print " selected";
		$s = 1;
	}
	print ">\n";
	if($i > 0 && length($server[$i]->{server_name}) > 0) {
		printf "%s - %s", $server[$i]->{server_name}, $server[$i]->{server_id};
	} else {
		print "&nbsp;"
	}
	print "</option>\n";
}
print "</select>\n";

print "</td><td width=10%>\n";
print "VIP:\n";
print "</td><td>\n";
print "<select name='vip'>\n";
print "<option value=\"0\" id=\"\"";
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["App Server:", $q->textfield(-name=>"app_server", -size=>10)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["AS Version:", $q->textfield(-name=>"as_version", -size=>6)]);
print "</tr>\n";
print "<tr>\n";
print "<td>Clustered?</td>\n";
print "<td>\n";
print $q->radio_group(-name=>'clustered', -values=>['Y', 'N']);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td>High Availability?</td>\n";
print "<td>\n";
print $q->radio_group(-name=>'high_avail', -values=>['Y', 'N']);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print $q->td("Deploy Location:");
print $q->td({colspan=>5}, $q->textfield(-size=>100, -name=>"deploy_location"));
print "</tr>\n";
print "<tr><td>\n";
print "State:\n";
print "</td><td>\n";
print $q->popup_menu(-name=>"state", -values=>["", "ASSIGNED","PLANNED","CONFIGURED","NOT USED","TEST"]);
print "</td></tr>\n";
print "<tr><td>\n";
print "Language:\n";
print "</td><td>\n";
#print "<br>2:cur_lang=$cur_lang<br>\n";
print "<select name='lang' class=fixed onChange=\"langVersion(0);\">\n";
#print "<select name='lang' class=fixed>\n";
$s = 0;
for($i = 0; $i <= $#lang; $i++) {
	if($i > 0 && $lang[$i]->{language} eq $lang[$i - 1]->{language}) {
		next;
	}
	if($i > 0 && length($lang[$i]->{language}) < 1) {
		next;
	}
	print "<option value=\"$lang[$i]->{language}\"";
	if($cur_lang eq $lang[$i]->{language} && $s == 0) {
		print " selected";
		$s = 1;
	}
	print ">\n";
	if($i > 0 && length($lang[$i]->{language}) > 0) {
		print "$lang[$i]->{language}";
	} else {
		print "&nbsp;"
	}
	print "</option>\n";
}
print "</select>\n";
print "</td>\n";
print "<td>\n";
print "Language Version:\n";
print "</td>\n";
print "<td>\n";
print "<select name='langversion' class=fixed>\n";
if($cur_lang eq "") {
	print "<option>";
	print "Select Version";
	print "</option>\n";
	print "</select>\n";
} else {
	$lv = $q->param('langversion');
	for($i = 1; $i < $#langdata; $i++) {
		if($cur_lang eq $langdata[$i]->{language}) {
			$t_version = $langdata[$i]->{version};
			if(length($t_version) < 1) {
				$t_version = $NONESTR;
			}
			print "<option value=$langdata[$i]->{lang_id}";
			if($lv == $langdata[$i]->{lang_id}) {
				print " selected";
			}
			print ">$t_version</option>\n";
			#print "\t\tdocument.editFields.langversion[$k] = new Option('$t_version', '$langdata[$j]->{lang_id}', false, true);\n";
		}
	}
}

print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Comp Env ID:", $q->textfield(-name=>"comp_env_id", -readonly=>"readonly")]);
print "</tr>\n";
print "</table>\n";
print "\n<input type=\"hidden\" name=\"language_id\" value=\"0\" />\n";
print "\n<br>\n";
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
#print "&nbsp;";
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

#
# If $q->param() equals "", then it's either the
# first time through, or something similar.
#
#
# Okay, user clicked on search...
#
if ($inputtype eq 'Search') {
	#
	# Note, we're not searching on comp_id, even if
	# one exists in the parameter list.
	#
	my $name = $q->param('gen_name');
	my $type = $q->param('gen_type');
	my $id = $q->param('gen_system_id');
	my $env = $q->param('gen_env');
	
	#
	# Since we're not sure what, if anything, has been
	# entered, $a is set to 1 when we've started a
	# search string.  That way we know we've got to
	# put an 'and' in front of the next parts.
	#
	my $a = 0;
	#
	# Build while string
	# $search_string2 will only be used if no rows are 
	# returned from the first query.
	#
	# Tell oracle to translate to all lower
	# case so that we're doing a case insensitive
	# search.
	#
	if(length($name) > 0) {
		$search_string = "lower(cmdb_comp_gen.comp_name) like lower('%$name%')";
		$search_string2 = "lower(comp_name) like lower('%$name%')";
		$a = 1;
	}
	if(length($type) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(cmdb_comp_gen.comp_type) like lower('%$type%')";
			$search_string2 = $search_string . " and lower(comp_type) like lower('%$type%')";
		} else {
			$search_string = "lower(cmdb_comp_gen.comp_type) like lower('%$type%')";
			$search_string2 = "lower(comp_type) like lower('%$type%')";
			$a = 1;
		}
	}
	if(length($id) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(cmdb_comp_gen.system_id) like lower('%$id%')";
			$search_string2 = $search_string . " and lower(system_id) like lower('%$id%')";
		} else {
			$search_string = "lower(cmdb_comp_gen.system_id) like lower('%$id%')";
			$search_string2 = "lower(system_id) like lower('%$id%')";
			$a = 1;
		}
	}
	if(length($env) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(env) like lower('%$env%')";
		} else {
			$search_string = "lower(env) like lower('%$env%')";
			$a = 1;
		}
	}
	#
	# Note that this if guarentees that the search_string 
	# is not null.  That's important, because if search_string
	# is null, our query fails because of the "where" keyword in
	# insstr.
	#
	if($a > 0) {
		$search_string = $search_string . " and cmdb_comp_gen.comp_id = cmdb_comp_env.comp_id"
	} else {
		$search_string = $search_string . " cmdb_comp_gen.comp_id = cmdb_comp_env.comp_id"
	}	
	#
	# We'll get a count of the rows we'll get back first, so
	# that if the number of rows returned is zero, we've got
	# a cleaner output.
	#
	$insstr = "select count(*) from cmdb_comp_gen, cmdb_comp_env where $search_string order by comp_name, env, comp_type, cmdb_comp_env.comp_env_id";
#print "<p>$insstr</p>\n";
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	$temp = $getTbl->fetchrow();
	if($temp > 0) {
		$insstr = "select * from cmdb_comp_gen, cmdb_comp_env where $search_string order by comp_name, env, comp_type, cmdb_comp_env.comp_env_id";
#print "<p>$insstr</p>\n";
		$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
	        $getTbl->execute();
	
		#
		# Create/populate the table we got from
		# the user's search parameters.
		#
		print "<table border=1>\n";
		print "<tr>\n";
		print "<th>Comp Env ID</th>\n";
		print "<th>Comp ID</th>\n";
		print "<th>Comp Name</th>\n";
		print "<th>Comp Type</th>\n";
		print "<th>System ID</th>\n";
		print "<th>Environment</th>\n";
		#print "<th>Version</th>\n";
		#print "<th>Server</th>\n";
		print "<th>App Server</th>\n";
		#print "<th>AS Version</th>\n";
		#print "<th>Clustered?</th>\n";
		print "<th>CL</th>\n";
		#print "<th>High Avail?</th>\n";
		print "<th>HA</th>\n";
		print "<th>Deploy Location</th>\n";
		#print "<th>VIP</th>\n";
		#print "<th>State</th>\n";
		print "</tr>\n";
		$o_comp_name = "";
		$o_comp_type = "";
		$o_comp_id_gen = "";
		$o_system_id = "";
		$o_env = "";
		$o_version = "";
		$o_app_server = "";
		$o_as_version = "";
		$o_clustered = "";
		$o_high_avail = "";
		$o_deploy_location = "";
		$o_state = "";
	
		# 
		# We collect a lot more data than we show,
		# but that data gets put into the anchor link
		# and used to fill the forms in.
		#
		while (($comp_id, $comp_name, $comp_type, $system_id, $comp_env_id, $comp_id_gen, $env, $version, $server_id, $app_server, $as_version, $clustered, $high_avail, $deploy_location, $vp_id, $state) = $getTbl->fetchrow()) {
			for($i = 0; $i < $#lang; $i++) {
				if($lang[$i]->{comp_env_id} == $comp_env_id) {
					$lang_lang_id = $lang[$i]->{lang_id};
					$lang_language = $lang[$i]->{language};
					$lang_version = $lang[$i]->{version};
					$lang_comp_env_id = $lang[$i]->{comp_env_id};
					last;
				}
			}
			#
			# This is simply to cut down on the number of lines
			# returned in the search.  If everything is equal except
			# the comp_env_id, then don't show the line.  We don't
			# really care about the comp_env_id at this point...
			# (well, maybe we do, but only if we're deleting a line
			# in the database.  Okay, I lied, it's not everything
			# but the comp_id and comp_env_id, it's selected info.
			# Hopefully it's the most important info...
			#
#			if($o_comp_name eq $comp_name && $o_comp_type eq $comp_type && $o_system_id == $system_id && $o_env eq $env && $o_app_server eq $app_server && $o_clustered eq $clustered && $o_high_avail eq $high_avail && $o_deploy_location eq $deploy_location) {
#				next;
#			}
			$o_comp_name = $comp_name;
			$o_comp_type = $comp_type;
			$o_system_id = $system_id;
			$o_env = $env;
			$o_app_server = $app_server;
			$o_as_version = $as_version;
			$o_clustered = $clustered;
			$o_high_avail = $high_avail;
			$o_deploy_location = $deploy_location;
	
			print "<tr>\n";
			if(!defined($vp_id)) {
				$vp_id = 0;
			}
			if($lang_lang_id < 1 || $lang_lang_id eq "") {
				$lang_lang_id = 0;
			}
			print "<td><a href=\"#\" onClick=\"fillForm($comp_env_id, $comp_id, '$comp_name', '$comp_type', '$system_id', '$env', '$app_server', '$clustered', '$high_avail', '$deploy_location', '$comp_type', $comp_id, $server_id, $vp_id, '$state', '$lang_language', $lang_lang_id)\">$comp_env_id</a></td>\n";
			print "<td>$comp_id</td>\n";
			print "<td>$comp_name</td>\n";
			print "<td>$comp_type</td>\n";
			print "<td>$system_id</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			#if(length($version) < 1) { $version = "&nbsp;"; }
			#print "<td>$version</td>\n";
			if(length($app_server) < 1) { $app_server = "&nbsp;"; }
			print "<td>$app_server</td>\n";
			#if(length($as_version) < 1) { $as_version = "&nbsp;"; }
			#print "<td>$as_version</td>\n";
			print "<td>$clustered</td>\n";
			print "<td>$high_avail</td>\n";
			if(length($deploy_location) < 1) { $deploy_location = "&nbsp;"; }
			print "<td>$deploy_location</td>\n";
			#if(length($state) < 1) { $state = "&nbsp;"; }
			#print "<td>$state</td>\n";
			print "</tr>\n";
		}
		$getTbl->finish;
		print "</table>\n";
	} else {
	#
	# Else the join returned no rows, so now lets
	# try just the gen table.
	#
		print "<p><b>No Data Found in CMDB_COMP_ENV, trying CMDB_COMP_GEN.</b></p>\n";
		$insstr = "select * from cmdb_comp_gen where $search_string2 order by comp_name, comp_type, system_id";
#print "<p>$insstr</p>\n";
		$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
	        $getTbl->execute();

		print "<table border=1>\n";
		print "<tr>\n";
		print "<th>Comp ID</th>\n";
		print "<th>Comp Name</th>\n";
		print "<th>Comp Type</th>\n";
		print "<th>System ID</th>\n";
		print "</tr>\n";
		$o_comp_name = "";
		$o_comp_type = "";
		$o_comp_id_gen = "";
		while (($comp_id, $comp_name, $comp_type, $system_id) = $getTbl->fetchrow()) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm(0, $comp_id, '$comp_name', '$comp_type', '$system_id', '', '', '', '', '', '', 0, 0, 0, '', '', 0)\">$comp_id</a></td>\n";
			print "<td>$comp_name</td>\n";
			print "<td>$comp_type</td>\n";
			print "<td>$system_id</td>\n";
			print "</tr>\n";
		}
		$getTbl->finish;
		print "</table>\n";

	}

} elsif ($inputtype eq 'Create New') {
	#
	# We try to do two things here.  We create an entry
	# in the cmdb_comp_env table using the data we have..
	# that should always happen.  And we try to create an
	# entry into the cmdb_language table.  We can't
	# guarentee that, because we can't guarentee that 
	# the user entered a language.
	#
	my $comp_id = $q->param('gen_comp_id');
	my $env = $q->param('env');
	my $version = $q->param('version');
	my $server_id = $q->param('server');
	my $app_server = $q->param('app_server');
	my $as_version = $q->param('as_version');
	my $clustered = $q->param('clustered');
	my $high_avail = $q->param('high_avail');
	my $deploy_location = $q->param('deploy_location');
	my $vp_id = $q->param('vip');
	my $state = $q->param('state');
	my $lang = $q->param('lang');
	my $langversion = $q->param('langversion');
	my $comp_env_id = $q->param('comp_env_id');

	$err = 0;

	if(!defined($comp_id) || $comp_id eq "") {
		$comp_id = 0;
	}
	if(!defined($server_id) || $server_id eq "") {
		$server_id = 0;
	}
	if(!defined($vp_id) || $vp_id eq "") {
		$vp_id = 0;
	}

	if(length($comp_id) == 0 || length($env) < 1 || $server_id == 0) {
                print "<p><b>Error: You must select a Compenent Name, an Environment, and a Server.</b></p>\n";
		$err = 1;
        }

	if(length($version > 8)) {
		print "<p><b>Error: Length of VERSION must not exceed 8 characters.</b></p>\n";
		$err = 1;
	}

	if(length($as_version > 6)) {
		print "<p><b>Error: Length of AS VERSION must not exceed 6 characters.</b></p>\n";
		$err = 1;
	}

	if(length($app_server > 10)) {
		print "<p><b>Error: Length of APP SERVER must not exceed 10 characters.</b></p>\n";
		$err = 1;
	}

	if(length($deploy_location > 120)) {
		print "<p><b>Error: Length of DEPLOY LOCATION must not exceed 120 characters.</b></p>\n";
		$err = 1;
	}

	if($err == 1) {
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }

	$insstr = "select comp_env_id from cmdb_comp_env where comp_id=$comp_id and env='$env' and server_id=$server_id and deploy_location='$deploy_location'";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
        $i = 0;
        while ($temp = $getTbl->fetchrow()) {
#print "<p><b>comp_env_id=$temp</b></p>\n";
                $i++;
        }
	$getTbl->finish;
	if($i != 0) {
		print "<p><b>Duplicate detected, row not inserted.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	#
	# If vp_id is zero, then we get an integrity constraint error
	# from Oracle, so if it is zero, we don't try to insert any
	# vp_id at all, avoiding the problem.
	#
	if($vp_id > 0) {
		$insstr = "insert into cmdb_comp_env (comp_id, env, version, server_id, app_server, as_version, clustered, high_avail, deploy_location, vp_id, state) values ($comp_id, '$env', '$version', $server_id, '$app_server', '$as_version', '$clustered', '$high_avail', '$deploy_location', $vp_id, '$state')";
	} else {
		$insstr = "insert into cmdb_comp_env (comp_id, env, version, server_id, app_server, as_version, clustered, high_avail, deploy_location, state) values ($comp_id, '$env', '$version', $server_id, '$app_server', '$as_version', '$clustered', '$high_avail', '$deploy_location', '$state')";
	}

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_COMP_ENV completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_COMP_ENV failed:</p>\n";
		print "<p>$insstr</p>\n";
		print "<p>$DBI::errstr</p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	#
	# Okay, we've created a new entry in the
	# cmdb_comp_env table. We should be able to
	# get the comp_env_id by selecting for it using
	# the entries we just stuck in the table.  We
	# need it to put into the cmdb_language table.
	#
	$search_string = "where comp_id=$comp_id";
	if(length($env) > 0) {
		$search_string = $search_string . " and env='$env'";
	}
	if(length($version) > 0) {
		$search_string = $search_string . " and version='$version'";
	}
	if($server_id > 0) {
		$search_string = $search_string . " and server_id=$server_id";
	}
	if(length($app_server) > 0) {
		$search_string = $search_string . " and app_server='$app_server'";
	}
	if(length($as_version) > 0) {
		$search_string = $search_string . " and as_version='$as_version'";
	}
	$search_string = $search_string . " and clustered='$clustered'";
	$search_string = $search_string . " and high_avail='$high_avail'";
	if(length($deploy_location) > 0) {
		$search_string = $search_string . " and deploy_location='$deploy_location'";
	}
	if($vp_id > 0) {
		$search_string = $search_string . " and vp_id=$vp_id";
	}
	if(length($state) > 0) {
		$search_string = $search_string . " and state='$state'";
	}
	$insstr = "select comp_env_id from cmdb_comp_env $search_string";

	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$comp_env_id = $getTbl->fetchrow();
	$getTbl->finish;
#print "<!--\n";
#print "<script type=\"text/javascript\">\n";
#print "document.editFields.comp_env_id.value = 'testing';\n";
#print "</script>\n";
#print "-->\n";



	if(($comp_env_id == 0 || $comp_env_id eq "") && length($lang) > 0) {
		print "<p>Can't create the language table entry -- can't find the COMP_ENV_ID.</p>\n";
		print "<p>$insstr</p>\n";
		print "<p>$DBI::errstr</p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}
	

	#
	# langversion is badly named.  It's really the
	# lang_id entry from the language table.
	#
	if($langversion == 0 || $langversion eq "") {
		print "<p>Can't create the language table entry -- no language ID.</p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "select version from cmdb_language where lang_id=$langversion";
	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$tversion = $getTbl->fetchrow();
	$getTbl->finish;

	$insstr = "insert into cmdb_language (language, version, comp_env_id) values ('$lang', '$tversion', $comp_env_id)";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_LANGUAGE completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_LANGUAGE failed:</p>\n";
		print "<p>$insstr</p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}


} elsif ($inputtype eq 'Update') {
	my $comp_id = $q->param('gen_comp_id');
	my $env = $q->param('env');
	my $version = $q->param('version');
	my $server_id = $q->param('server');
	my $app_server = $q->param('app_server');
	my $as_version = $q->param('as_version');
	my $clustered = $q->param('clustered');
	my $high_avail = $q->param('high_avail');
	my $deploy_location = $q->param('deploy_location');
	my $vp_id = $q->param('vip');
	my $state = $q->param('state');
	my $lang = $q->param('lang');
	my $langversion = $q->param('langversion');
	my $comp_env_id = $q->param('comp_env_id');
	my $lang_id = $q->param('language_id');
	my $err = 0;

	if(length($version) > 8) {
		print "<p><b>Error: Length of VERSION must not exceed 8 characters.</b></p>\n";
		$err = 1;
	}

	if(length($as_version) > 6) {
		print "<p><b>Error: Length of AS VERSION must not exceed 6 characters.</b></p>\n";
		$err = 1;
	}

	if(length($app_server) > 10) {
		print "<p><b>Error: Length of APP SERVER must not exceed 10 characters.</b></p>\n";
		$err = 1;
	}

	if(length($deploy_location) > 120) {
		print "<p><b>Error: Length of DEPLOY LOCATION must not exceed 120 characters.</b></p>\n";
		$err = 1;
	}

	if($err == 1) {
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }

	if($comp_env_id < 1) {
		print"<p><b>COMP_ENV_ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		if($vp_id > 0 && $vp_id ne "") {
			$vip = " vp_id=$vp_id, ";
		} else {
			$vip = "";
		}

		$insstr = "update cmdb_comp_env set comp_id=$comp_id, env='$env', version='$version', server_id=$server_id, app_server='$app_server', as_version='$as_version', clustered='$clustered', high_avail='$high_avail', deploy_location='$deploy_location', $vip state='$state' where comp_env_id=$comp_env_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update of CMDB_COMP_ENV completed.</p>\n";
		} else {
			print "<p>Update of CMDB_COMP_ENV failed:</p>\n";
			print "<p>$insstr</p>\n";
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}

		#
		# Language isn't required, so if we don't
		# have one here, then just exit.  Else add
		# to the table.
		#
		if(length($lang) < 1) {
			print $q->end_html();
			print "<p>CMDB_LANGUAGE not updated:  Language is empty.<p>\n";
			$dbh->disconnect;
			exit 0;
		}

		if($lang_id < 1) {
			print $q->end_html();
			print "<p>CMDB_LANGUAGE not updated:  No valid language ID.<p>\n";
			$dbh->disconnect;
			exit 0;
		}

		$insstr = "select language from cmdb_language where lang_id=$lang_id and comp_env_id=$comp_env_id";
		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$getTbl->execute();
		$k = 0;
		while($t_comp_env_id = $getTbl->fetchrow()) {
			$k++;
		}
		$getTbl->finish;

		if($k == 0) {
			print "<p>Update of CMDB_LANGUAGE failed:</p>\n";
			print "<p>Can't find an entry with lang_id=$lang_id and comp_env_id=$comp_env_id<p>\n";
			print "<p>$insstr</p>\n";
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}
		if($k > 1) {
			print "<p>Update of CMDB_LANGUAGE failed:</p>\n";
			print "<p>Found more than one entry with lang_id=$lang_id and comp_env_id=$comp_env_id<p>\n";
			print "<p>$insstr</p>\n";
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}

		$insstr = "update cmdb_language set language='$lang', version='$tversion' where comp_env_id=$comp_env_id and lang_id=$lang_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update of CMDB_LANGUAGE completed .</p>\n";
		} else {
			print "<p>Update of CMDB_LANGUAGE failed:</p>\n";
			print "<p>$insstr</p>\n";
			print $q->end_html();
			$dbh->disconnect;
			exit 0;
		}

	}

} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
	my $i;
	my $j;
	my $user;
	my $used;
	my $comp_env_id = $q->param('comp_env_id');
	if($comp_env_id < 1 || $comp_env_id == "") {
		print"<p><b>COMP_ENV_ID is zero.  Can't delete that.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	#
	# We need to remove the child table entries before
	# removing the main cmdb_comp_env entry, or we'll
	# get an Oracle constraint error.
	#

	$insstr = "select comp_env_id_user, comp_env_id_used from cmdb_comp_uses where comp_env_id_user=$comp_env_id or comp_env_id_used=$comp_env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while(($user[$i], $used[$i]) = $getTbl->fetchrow()) {
		$i++;
	}
	if($i > 0) {
		for($j = 0; $j < $i; $j++) {
			$insstr = "delete from cmdb_comp_uses where comp_env_id_user=$user[$j] and comp_env_id_used=$used[$j]";
			#print "<br>$insstr</br>\n";
			$getTbl = $dbh->prepare($insstr);
			$getTbl->execute();
			$getTbl->finish;
		}
		if($j > 0) {
			printf "<p>$j row%s removed from CMDB_COMP_USES</p>\n", $j > 1?"s":"";
		} else {
			print "<p>No rows removed from CMDB_COMP_USES</p>\n";
		}
	}

	$insstr = "select comp_env_id_includer, comp_env_id_included from cmdb_includes where comp_env_id_includer=$comp_env_id or comp_env_id_included=$comp_env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while(($user[$i], $used[$i]) = $getTbl->fetchrow()) {
		$i++;
	}
	if($i > 0) {
		for($j = 0; $j < $i; $j++) {
			$insstr = "delete from cmdb_includes where comp_env_id_includer=$user[$j] and comp_env_id_included=$used[$j]";
			#print "<br>$insstr</br>\n";
			$getTbl = $dbh->prepare($insstr);
			$getTbl->execute();
			$getTbl->finish;
		}
		if($j > 0) {
			printf "<p>$j row%s removed from CMDB_INCLUDES</p>\n", $j > 1?"s":"";
		} else {
			print "<p>No rows removed from CMDB_INCLUDES</p>\n";
		}
	}

	$insstr = "select lang_id from cmdb_language where comp_env_id=$comp_env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while(($user[$i]) = $getTbl->fetchrow()) {
		$i++;
	}
	if($i > 0) {
		for($j = 0; $j < $i; $j++) {
			$insstr = "delete from cmdb_language where lang_id=$user[$j] and comp_env_id=$comp_env_id";
			#print "<br>$insstr</br>\n";
			$getTbl = $dbh->prepare($insstr);
			$getTbl->execute();
			$getTbl->finish;
		}
		if($j > 0) {
			printf "<p>$j row%s removed from CMDB_LANGUAGE</p>\n", $j > 1?"s":"";
		} else {
			print "<p>No rows removed from CMDB_LANGUAGE</p>\n";
		}
	}

	$insstr = "select db_id from cmdb_db_uses where comp_env_id=$comp_env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while(($user[$i]) = $getTbl->fetchrow()) {
		$i++;
	}
	if($i > 0) {
		for($j = 0; $j < $i; $j++) {
			$insstr = "delete from cmdb_db_uses where db_id=$user[$j] and comp_env_id=$comp_env_id";
			#print "<br>$insstr</br>\n";
			$getTbl = $dbh->prepare($insstr);
			$getTbl->execute();
			$getTbl->finish;
		}
		if($j > 0) {
			printf "<p>$j row%s removed from CMDB_DB_USES</p>\n", $j > 1?"s":"";
		} else {
			print "<p>No rows removed from CMDB_DB_USES</p>\n";
		}
	}

	$insstr = "delete from cmdb_comp_env where comp_env_id=$comp_env_id";

	#print "<br>$insstr<br>\n";
	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Delete from CMDB_COMP_ENV completed.</p>\n";
	} else {
		print "<p>Delete from CMDB_COMP_ENV failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


