#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
$db = "P067";
# For dev
#$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
#$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Print the headers, and create the two Javascript
# functions we use.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(lang_id, language, version) {
	document.editFields.language.value = language;
	document.editFields.version.value = version;
	document.editFields.lang_id.value = lang_id;
}
function clearForm() {
	document.editFields.language.value = '';
	document.editFields.version.value = '';
	document.editFields.lang_id.value = '';
}
function confirmDelete() {
        var answer = confirm(\"Delete this language record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}
\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_LANGUAGE');

print "<b>Edit Language Table</b><br>\n";

print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";

print "
<p>
This form is for adding or deleting a new language and version.  It may
not be used to edit any row that has an associated component environment
ID.  Use the form for editing the CMDB_COMP_ENV table to link a language
with an component ID.
</p>
";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# inputtype holds the value of the submit button
# that was clicked, either Search, Update or
# Create New.
#
$inputtype = $q->param('inputtype');
$language = $params{'language'};
$version = $params{'version'};
$lang_id = $params{'lang_id'};

$insstr = "insert into cmdb_language (language, version) values ('$language', '$version')";

#print "<br>$insstr<br>\n";

$do_delete = $q->param('DELETE');

# Output the form
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print $q->table(
	$q->Tr(
	[
		$q->td(["Language:", $q->textfield(-name=>"language")]),
		$q->td(["Version:", $q->textfield(-name=>"version")]),
		$q->td(["Language ID:", $q->textfield(-name=>"lang_id", -readonly=>"readonly")]),
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
#print $q->submit(-value => "Update", -name => "inputtype");
#print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

# If $q->param() equals "", this is the first time through.
$inputtype = $q->param('inputtype');
#if ($q->param() ne "") {
if ($inputtype eq 'Search') {
	my $language = $q->param('language');
	my $version = $q->param('version');
	my $a = 0;
	#
	# Build while string
	#
	if(length($language) > 0) {
		$search_string = "lower(language) like lower('%$language%')";
		$a = 1;
	}
	if(length($version) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(version) like lower('%$version%')";
		} else {
			$search_string = $search_string . " lower(version) like lower('%$version%')";
			$a = 1;
		}
	}
	if($a) {
		$search_string = " where (comp_env_id is null or comp_env_id=0) and " . $search_string;
	} else {
		$search_string = " where (comp_env_id is null or comp_env_id=0) ";
	}
        $getTbl = $dbh->prepare("select lang_id, language, version, comp_env_id from cmdb_language $search_string order by lang_id, language, version") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Language ID</th>\n";
	print "<th>Language Name</th>\n";
	print "<th>Version</th>\n";
	print "</tr>\n";
	while (($lang_id, $language, $version, $env_id) = $getTbl->fetchrow()) {
		if(length($language) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm($lang_id, '$language', '$version', '$env_id')\">$lang_id</a></td>\n";
			print "<td>$language</td>\n";
			#
			# If you put a data element in with no value,
			# ie. "<td></td>" it messes up the way the 
			# table looks, so if the os length is zero,
			# put a non-breakable space in to make a real
			# table cell.
			#
			if(length($version) < 1) { $version = "&nbsp;" }
			print "<td>$version</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	$language = $params{'language'};
	$version = $params{'version'};
	$language  =~ s/^\s+|\s+$//g;
	$version  =~ s/^\s+|\s+$//g;


        if(length($version) < 1) {
                $vstring = "version is null";
        } else {
                $vstring = "version='$version'";
        }
	$insstr = "select language from cmdb_language where lower(language)=lower('$language') and $vstring";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while($this_lang = $getTbl->fetchrow()) {
		$i++;
	}
	$getTbl->finish;

	if($i != 0) {
		print "<p><b>Duplicate entry ($lang_id) detected.  No changes made to table.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit;
	}
	$language = uc($language);
	$version = uc($version);
	$insstr = "insert into cmdb_language (language, version) values ('$language', '$version')";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_LANGUAGE completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_LANGUAGE failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

#} elsif ($inputtype eq 'Update') {
#	$language = $params{'language'};
#	$version = $params{'version'};
#	$lang_id = $params{'lang_id'};
#	if($lang_id < 1) {
#		print"<p><b>LANG ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
#	} else {
#		$insstr = "update cmdb_language set language='$language', version='$version' where lang_id=$lang_id";
#
#		#print "<br>$insstr<br>\n";
#
#		$getTbl = $dbh->prepare($insstr);
#		$i = $getTbl->execute();
#		$getTbl->finish;
#		if($i != 0) {
#			print "<p>Update to CMDB_LANGUAGE completed.</p>\n";
#		} else {
#			print "<p>Update to CMDB_LANGUAGE failed:</p>\n";
#			print "<p>$insstr</p>\n";
#		}
#	}
#
} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $lang_id = $q->param('lang_id');
        if($lang_id < 1 || $lang_id == "") {
                print"<p><b>Language ID is zero.  Can't delete that.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }
        
	if(length($version) < 1) {
		$vstring = "version is null";
	} else {
		$vstring = "version='$version'";
	}
        $insstr = "select comp_env_id from cmdb_language where language='$language' and $vstring";
	$i = 0;
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	while($comp_env_id = $getTbl->fetchrow()) {
		if ($comp_env_id > 0) {
			$i++;
		}
	}

        $getTbl->finish;
	if($i != 0) {
		print "<p><b>A comp_env_id still exists for this table. No changes made.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit;
        }
        $insstr = "delete from cmdb_language where lang_id=$lang_id";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_LANGUAGE completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_LANGUAGE failed:</p>\n";
                print "<p>$insstr</p>\n";
        }

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;
