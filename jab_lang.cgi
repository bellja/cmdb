#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1" );
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard" );
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Print the headers, and create the two Javascript
# functions we use.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(lang_name, version, lang_id) {
	document.editFields.lang.value = lang_name;
	document.editFields.version.value = version;
	document.editFields.lang_id.value = lang_id;
}
function clearForm() {
	document.editFields.lang.value = '';
	document.editFields.os_version.value = '';
	document.editFields.lang_id.value = '';
}
\n";
print "</script>\n";"</script>\n";


%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# inputtype holds the value of the submit button
# that was clicked, either Search, Update or
# Create New.
#
$inputtype = $q->param('inputtype');

#
# Get a list of OS names
#
#$getTbl = $dbh->prepare(" unique LANG_ID from cmdb_language order by LANG_ID") or die "Error preparing statement: $DBI::errstr\n";
#$getTbl->execute();
#$i = 0;
#while ($a_os[$i] = $getTbl->fetchrow()) {
#	$i++;
#}
#
# We need it in a hash, not an array, so
# map it into a hash
#
#%h_os = map { $_, $_ } @a_os;
#$getTbl->finish;

# Output the form
print $q->start_form(-name=>"editFields");
print $q->table(
	$q->Tr(
	[
		#$q->td(["Environment", $q->popup_menu(-name=>"env", -values=>["ALL","PROD", "DEV","QA","PREP","SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"])]),
        #$q->td(["Environment", $q->popup_menu(-name=>"env", -values=>["PROD", "SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"])]),
		$q->td(["Language", $q->textfield(-name=>"lang", -values=>\%h_os)]),
		$q->td(["Version", $q->textfield(-name=>"version")]),
		$q->td(["Language ID", $q->textfield(-name=>"lang_id")]),
        #$q->td(["Server ID", $q->textfield(-name=>"server_id", -readonly=>"readonly", -value=>"")])
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print $q->end_form();

# If $q->param() equals "", this is the first time through.
$inputtype = $q->param('inputtype');
#if ($q->param() ne "") {
if ($inputtype eq 'Search') {
	my $lang_name = $q->param('lang_name');
    #my $lang_id = $q->param('lang_id');
	my $version = $q->param('version');
	#
	# Build while string
	#
	$search_string = "lang_name='$lang_name'";
	if(length($lang_name) > 0) {
		#
		# Translate into all caps, a require of Herb.
		# This is the old way, I think:
		# $lang = uc($lang)
		# works as well.
		#
		$lang_name =~ tr/a-z/A-Z/;
		$search_string = $search_string . " and lang_name like '%$lang%'"
	}
	if(length($version) > 0) {
		$search_string = $search_string . " and version like '%$version%'";
	}
        $getTbl = $dbh->prepare("select * from cmdb_language where $search_string order by lang_id, lang_name") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Language ID</th>\n";
	print "<th>Language Name</th>\n";
	print "<th>Version</th>\n";
    print "<th>Comp Env</th>\n";
	print "</tr>\n";
	while (($lang_id, $lang_name, $version, $env_id) = $getTbl->fetchrow()) {
		if(length($lang_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm($lang_id, '$lang_name', '$version', '$env_id')\">$lang_id</a></td>\n";
			print "<td>$lang_name</td>\n";
			#
			# If you put a data element in with no value,
			# ie. "<td></td>" it messes up the way the 
			# table looks, so if the os length is zero,
			# put a non-breakable space in to make a real
			# table cell.
			#
			if(length($version) > 0) {
				print "<td>$version</td>\n";
			} else {
				print "<td>&nbsp</td>\n";
			}
			print "<td>$env_id</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	$lang_name = $params{'lang_name'};
	$version = $params{'version'};
	$insstr = "insert into cmdb_language (lang_name, version) values ('$lang', '$version')";

	print "<br>$insstr<br>\n";

#	$getTbl = $dbh->prepare($insstr);
#	$i = $getTbl->execute();
#	$getTbl->finish;
#	if($i != 0) {
#		print "<p>Insert completed.</p>\n";
#	} else {
#		print "<p>Insert failed:</p>\n";
#		print "<p>$insstr</p>\n";
#	}

} elsif ($inputtype eq 'Update') {
	$lang_name = $params{'lang_name'};
	$version = $params{'version'};
    #$lang_id = $params{'lang_id'};
	if($lang_id < 1) {
		print"<p><b>LANG ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		$insstr = "update cmdb_language set lang_name='$lang_name', version='$version' where lang_id=$lang_id";

		print "<br>$insstr<br>\n";

#		$getTbl = $dbh->prepare($insstr);
#		$i = $getTbl->execute();
#		$getTbl->finish;
#		if($i != 0) {
#			print "<p>Insert completed.</p>\n";
#		} else {
#			print "<p>Insert failed:</p>\n";
#			print "<p>$insstr</p>\n";
#		}
	}

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;

