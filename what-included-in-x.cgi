#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: what-included-in-x.cgi                                           #
# Author: Herb Gregory                                                     #
# Date: Aug 5, 2011                                                        #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

###############################################
# Parse Commandline                           #
###############################################

#if ($#ARGV == 2) {
#        $db   = $ARGV[0];
#        $comp = $ARGV[1];
#        $env  = $ARGV[2];
#}
#else {
#        usage();
#}
$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

#$dbh = DBI->connect( $oraId, $login, $pass );
my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get all Software Components for PROD

my $getCurrentDomain = $dbh->prepare("
		SELECT  unique(cmdb_comp_gen.comp_name)
		FROM    cmdb_comp_gen, cmdb_comp_env
		WHERE   cmdb_comp_env.env = 'PROD'
		AND     cmdb_comp_gen.comp_id = cmdb_comp_env.comp_id
		AND     cmdb_comp_gen.comp_type = 'DomainArchive'
		ORDER BY comp_name
")
   or die "Error preparing getCurrentDomain statement: $DBI::errstr\n";

# Get the Component ID from the COMP_GEN table

my $getCompId = $dbh->prepare("
		SELECT  cmdb_comp_gen.comp_id
		FROM    cmdb_comp_gen
		WHERE   cmdb_comp_gen.comp_name= ?
")
    or die "Error preparing getCompId statement: $DBI::errstr\n";

# Get the Component Environment ID from COMP_ENV table

my $getCompEnvId = $dbh->prepare("
		SELECT cmdb_comp_env.comp_env_id
		FROM   cmdb_comp_env
		WHERE  cmdb_comp_env.comp_id = ?
		AND    cmdb_comp_env.env = ?
")
    or die "Error preparing getCompEnvId statement: $DBI::errstr\n";

# Get the Included from the INCLUDES table

my $getIncluded = $dbh->prepare("
		SELECT cmdb_includes.comp_env_id_included
		FROM   cmdb_includes
		WHERE  cmdb_includes.comp_env_id_includer = ?
		ORDER BY cmdb_includes.comp_env_id_included
")
    or die "Error preparing getIncluded statment: $DBI::errstr\n";

# Get the Component ID from  COMP ENV table

my $getCompIdfromEnv = $dbh->prepare("
		SELECT cmdb_comp_env.comp_id
		FROM   cmdb_comp_env
		WHERE  cmdb_comp_env.comp_env_id = ?
")
    or die "Error preparing getCompIdfromEnv statement: $DBI::errstr\n";

# Get the Component General information from the COMP GEN table

my $getCompGen = $dbh->prepare("
		SELECT cmdb_comp_gen.comp_name, cmdb_comp_gen.comp_type, cmdb_comp_gen.system_id
		FROM   cmdb_comp_gen
		WHERE  cmdb_comp_gen.comp_id = ?
")
    or die "Error preparing getCompGen statement: $DBI::errstr\n";

# Get Component Environment information from the COMP ENV table

my $getCompEnv = $dbh->prepare("
		  SELECT cmdb_comp_env.vp_id, cmdb_comp_env.server_id, cmdb_comp_env.env,
			 cmdb_comp_env.version, cmdb_comp_env.app_server, cmdb_comp_env.as_version,
			 cmdb_comp_env.clustered, cmdb_comp_env.high_avail,
			 cmdb_comp_env.deploy_location, cmdb_comp_env.state
		  FROM   cmdb_comp_env
		  WHERE  cmdb_comp_env.comp_env_id = ?
")
    or die "Error preparing getCompEnv statement: $DBI::errstr\n";

# Select server name from Server based on Server_ID from Comp_Env

my $getServer = $dbh->prepare("
		SELECT cmdb_server.server_name
		FROM   cmdb_server
		WHERE  server_id = ?
")
    or die "Error preparing getServerstatement: $DBI::errstr\n";

# Select vip and port from VIP_Port based on Server_ID from Comp_Env

my $getVipPort = $dbh->prepare("
		 SELECT cmdb_vip_port.vip, cmdb_vip_port.port
		 FROM   cmdb_vip_port
		 WHERE  server_id = ?
")
    or die "Error preparing getVipPortstatement: $DBI::errstr\n";

use strict;
#use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my  $env = "PROD";
my  $domain = "";
my  $domainname = "";
my  $compId = "";
my  $includer = "";
my  $includedCompName = "";
my  $includedCompId = "";
my  $includedCompType = "";
my  $includedSystemId = "";
my  $included = "";
my  $compEnv = "";
my  $version = "";
my  $server_id = "";
my  $serverName = "";
my  $appServer = "";
my  $asVersion = "";
my  $clustered = "";
my  $ha = "";
my  $deployLocation = "";
my  $vpId = "";
my  $vip = "";
my  $port = "";
my  $state = "";

###############################################
# Main                                        #
###############################################

# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What included in DomainArchive (Jboss domain): ";
    print $q->textfield(-name => "domainname");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();

# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

   # Gather and output the list of current Domain Archives in Prod

   print "Current list of Production Domain Archives:";
   print $q->br;

   $getCurrentDomain->execute();
       while (($domain) = $getCurrentDomain->fetchrow()) {
	    print $q->br;
	    print $domain;
   }
   $getCurrentDomain->finish;

} else {                                # Second time thru - get info for the specified component
    my $domainname = uc($q->param('domainname'));

    # Get the Component ID from the COMP GEN table
    $getCompId->execute($domainname);
    ($compId) = $getCompId->fetchrow();
    $getCompId->finish;

    if ($compId eq "") {    # If no COMP ID the COMP NAME input is probably not valid
	print "No COMP ID found for Domain $domainname\n";
	print "Processing stopped\n";
	exit 1;
    }

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>What included in $domainname in the Production Environment</title></head>";

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";

    # Output table headers line
    print "<TR><TH> COMP_NAME </TH><TH> COMP_ID </TH><TH> COMP_TYPE </TH><TH> SYSTEM_ID </TH><TH> COMP_ENV_ID </TH><TH> ENV </TH><TH> VERSION </TH><TH> SERVER_ID </TH><TH> SERVER_NAME </TH><TH> APP_SERVER </TH><TH> AS_VERSION </TH><TH> CLUSTERED </TH><TH> HA </TH><TH> DEPLOY_LOCATION </TH><TH> VP_ID </TH><TH> VIP </TH><TH> PORT </TH><TH> STATE </TH></TR>";

    # Get the Component Environment IDs for the Includer from the COMP ENV table
    $getCompEnvId->execute($compId, $env);

    # One Domain Archive may be deployed in multiple locations so there may be multiple "Includers" returned
    # Process each Includer
    while (($includer) = $getCompEnvId->fetchrow()) {

	# Get the "Includeds" from the Includes table
	$getIncluded->execute($includer);
	while (($included) = $getIncluded->fetchrow()) {

	    # Get and output info for one Included component
	    # Get COMP ID from COMP ENV table for the Included component
	    $getCompIdfromEnv->execute($included);
	    ($includedCompId) =  $getCompIdfromEnv->fetchrow();
	    $getCompIdfromEnv->finish;

	    # Get COMP GEN info
	    $getCompGen->execute($includedCompId);
	    ($includedCompName, $includedCompType, $includedSystemId) =  $getCompGen->fetchrow();
	    $getCompGen->finish;

	    # Get COMP ENV info
	    $getCompEnv->execute($included);
	    ($vpId, $server_id, $compEnv, $version, $appServer, $asVersion, $clustered, $ha, $deployLocation, $state) =  $getCompEnv->fetchrow();
	    $getCompEnv->finish;

	    # Get Server Name
	    $getServer->execute($server_id);
	    $serverName =  $getServer->fetchrow();
	    $getServer->finish;

	    # Get VIP and PORT
	    $getVipPort->execute($vpId);
	    ($vip, $port) =  $getVipPort->fetchrow();
	    $getVipPort->finish;

	    # Output one web page row
	    print "<TR><TD> $includedCompName </TD>";
	    print "<TD> $includedCompId </TD>";
	    print "<TD> $includedCompType </TD>";
	    print "<TD> $includedSystemId </TD>";
	    print "<TD> $included </TD>";
	    print "<TD> $compEnv </TD>";
	    print "<TD> $version </TD>";
	    print "<TD> $server_id </TD>";
	    print "<TD> $serverName </TD>";
	    print "<TD> $appServer </TD>";
	    print "<TD> $asVersion </TD>";
	    print "<TD> $clustered </TD>";
	    print "<TD> $ha </TD>";
	    print "<TD> $deployLocation </TD>";
	    print "<TD> $vpId </TD>";
	    print "<TD> $vip </TD>";
	    print "<TD> $port </TD>";
	    print "<TD> $state </TD></TR>";

	    # Clear variables for next pass
	    $includedCompName = "";
	    $includedCompId   = "";
	    $includedCompType = "";
	    $includedSystemId = "";
	    $included         = "";
	    $compEnv          = "";
	    $version          = "";
	    $server_id        = "";
	    $serverName       = "";
	    $appServer        = "";
	    $asVersion        = "";
	    $clustered        = "";
	    $ha               = "";
	    $deployLocation   = "";
	    $vpId             = "";
	    $vip              = "";
	    $port             = "";
	    $state            = "";
	}

    }
}

print "</TABLE></body></html>";

###############################################
# Closing                                     #
###############################################

#$dbh->commit();

$getCompEnvId->finish;

$dbh->disconnect;

exit 0;

###############################################
# Subroutines                                 #
###############################################

#sub usage {
#        $msg = $_[0];
#
#        print "what-included-in-x.pl: $msg\n";
#        print "Usage: DB Instance name, Component name, and environment are required inputs (e.g. what-included-in-x.pl P067 PropheticLoader PROD)\n";
#        print "No spaces or underscores in component name (e.g. PropheticDocumentService, ReferenceService, etc ...)\n";
#        print "Environment name must be in all CAPS (e.g. PROD, QA01, PREP02, etc ...)\n";
#
#        exit 1;
#}
