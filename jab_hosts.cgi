#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1" );
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard" );
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Print the headers, and create the two Javascript
# functions we use.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(host, env) {
	document.editFields.host.value = host;
    for(i = 0; i < document.editFields.env.length; i++) {
        if (document.editFields.env[1].value == env) {
            document.editFields.env[1].selected = true;
        }
    }

}
function clearForm() {
	document.editFields.hosts.value = '';
    document.editFields.env[0].selected = true;
}
\n";
print "</script>\n";"</script>\n";


%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# inputtype holds the value of the submit button
# that was clicked, either Search, Update or
# Create New.
#
$inputtype = $q->param('inputtype');

#
# Get a list of hostnames and their enviorment
#
$getTbl = $dbh->prepare("select unique env, hosts from cmdb_env_hosts order by env") or die "Error preparing statement: $DBI::errstr\n";
$getTbl->execute();
$i = 0;
while ($a_os[$i] = $getTbl->fetchrow()) {
	$i++;
}
#
# We need it in a hash, not an array, so
# map it into a hash
#
%h_os = map { $_, $_ } @a_os;
$getTbl->finish;

# Output the form
print $q->start_form(-name=>"editFields");
print $q->table(
	$q->Tr(
	[
		$q->td(["Hosts", $q->textfield(-size=>80, -name=>"hosts")]),
		$q->td(["Environment", $q->popup_menu(-name=>"env", -values=>["", "PROD", "SB01","DEV01","DEV02","DEV03","DEV04","QA01","QA02","QA03","QA04","PREP01","PREP02"])]),
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print $q->end_form();

# If $q->param() equals "", this is the first time through.
$inputtype = $q->param('inputtype');
#if ($q->param() ne "") {
if ($inputtype eq 'Search') {
	my $host = $q->param('hosts');
	my $env = $q->param('env');
    #my $os = $q->param('os');
	#
	# Build while string
	#
	$search_string = "env='$env'";
	if(length($host) > 0) {
		#
		# Translate into all caps, a require of Herb.
		# This is the old way, I think:
		# $host = uc($host)
		# works as well.
		#
		$host =~ tr/a-z/A-Z/;
		$search_string = $search_string . " and hosts like '%$host%'"
	}
        $getTbl = $dbh->prepare("select * from cmdb_env_hosts where $search_string order by env, hosts") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
    #print "<th>Server ID</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Hosts</th>\n";
	print "</tr>\n";
	while (($env, $host) = $getTbl->fetchrow()) {
		if(length($host) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm('$host', '$env')\">$env</a></td>\n";
			if(length($host) < 1) { $host = "&nbsp;" }
			print "<td>$host</td>\n";
			#
			# If you put an data element in with no value,
			# ie. "<td></td>" it messes up the way the 
			# table looks, so if the length is zero,
			# put a non-breakable space in to make a real
			# table cell.
			#
	    }
    }
	$getTbl->finish;
	print "</table>\n";
} elsif ($inputtype eq 'Create New') {
	$host = $params{'hosts'};
	$env = $params{'env'};
	$insstr = "insert into cmdb_env_hosts (hosts, env) values ('$host', '$env')";

	print "<br>$insstr<br>\n";

#	$getTbl = $dbh->prepare($insstr);
#	$i = $getTbl->execute();
#	$getTbl->finish;
#	if($i != 0) {
#		print "<p>Insert completed.</p>\n";
#	} else {
#		print "<p>Insert failed:</p>\n";
#		print "<p>$insstr</p>\n";
#	}

} elsif ($inputtype eq 'Update') {
	$host = $params{'hosts'};
	$env = $params{'env'};
    $server_id = $params{'server_id'};
    if($server_id < 1) {
    	print"<p><b>Server ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		$insstr = "update cmdb_env_hosts set hosts='$host', env='$env' where server_id=$server_id";

		print "<br>$insstr<br>\n";

#		$getTbl = $dbh->prepare($insstr);
#		$i = $getTbl->execute();
#		$getTbl->finish;
#		if($i != 0) {
#			print "<p>Insert completed.</p>\n";
#		} else {
#			print "<p>Insert failed:</p>\n";
#			print "<p>$insstr</p>\n";
#		}
	}
}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;
<EOF>
