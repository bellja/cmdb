#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

#
# All of this sets us up to access the database...
#

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1" );
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard" );
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# done setting up the database.


#
# This isn't written very consistantly.. sometimes I'm
# using OO ways of doing things, sometimes I use reg'lar
# ol' Perl.  This next line sets up a new CGI object,
# but I don't use it as consistantly as I should...
#
my $q = new CGI;

#
# This outputs some basic HTML stuff to start out page.
#
print $q->header();

#
# I don't like serif type faces.  This makes them all
# sans-serif.
#
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");

#
# These lines just print out the parameters that were passed
# into the script.  Just here so we can see what's going on,
# in prod, we'd comment them out.
#
%params = $q->Vars;
print "<pre>\n";
foreach $key (sort keys(%params)) {
  print "$key = $params{$key}<p>";
}
print "</pre>\n";

# Output the form
print $q->start_form();

print "<p>This form is for test purposes </b><i>only</i></b>.\n";
print "For this reason, please do not input\n";
print "a recognizable language name, and, specifically, do not input\n";
print "\"JAVA\" or \"C++\".  Instead use something that is obvious so\n";
print "that it can be easily removed later.</p>\n";

print $q->table(
	$q->Tr(
	[
		$q->td(["Server Name", $q->textfield(-name=>"server_name")]),
		# $q->td(["Enviorment", $q->textfield(-name=>"env")]),
		$q->td(["Operating System", $q->textfield(-name=>"os")]),
                $q->td(["OS version", $q->textfield(-name=>"os_version")]),	
	])
);
print $q->submit(-value => "Submit", -name => "inputtype");

print $q->end_form();

# If $q->param() equals "", this is the first time through.
#
# This actually isn't good enough, we need to check that the language
# parameter is not null as well, since it's a required parameter.
#
if ($q->param() ne "") {
	my $server_name = $q->param('server_name');
	# my $env = $q->param('env');
	my $os = $q->param('os');
	my $os_version = $q->param('os_version');
	
	#
	# General form of the insert statement:
	# insert into cmdb (Language, Version, Enviorment) values ($language, $version, $env);
	# There are other ways of doing an insert as well.
	#
	# We break it out so that if there is an error, we
	# can see what we tried to do.
	#
        $insst = "insert into cmdb_server (SERVER_NAME, OS, OS_VERSION) values ('$server_name','$os','$os_version')";
        $getTbl = $dbh->prepare($insst);

        $i = $getTbl->execute();

	$getTbl->finish;

	if($i != 0) {
		print "<p>Insert completed correctly</p>\n";
	} else {
		print "<p>$insst</p>\n";
		print "<p>Insert failed</p>\n";
	}
}

#
# This ends the HTML for the page.
#
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


