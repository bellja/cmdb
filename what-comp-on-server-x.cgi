#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: what-comp-on-server-x.cgi                                        #
# Author: Herb Gregory                                                     #
# Date: Aug 16, 2011                                                      #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get all Server Environment pairs

my $getServers = $dbh->prepare("
	      SELECT UNIQUE cmdb_server.server_name, cmdb_server.env
	      FROM cmdb_server
	      ORDER BY env
")
    or die "Error preparing getServers statement: $DBI::errstr\n";

# Get specific Server Environment pair

my $getServer = $dbh->prepare("
	      SELECT count(*)
	      FROM cmdb_server
	      WHERE cmdb_server.server_name = ? and cmdb_server.env = ?
")
    or die "Error preparing getServer statement: $DBI::errstr\n";

# Get Component Environment information from the COMP ENV table

my $getCompEnv = $dbh->prepare("
		SELECT  cmdb_comp_gen.comp_name, cmdb_comp_gen.comp_id, cmdb_comp_gen.comp_type,
			cmdb_comp_gen.system_id, cmdb_comp_env.comp_env_id, cmdb_comp_env.version,
			cmdb_comp_env.server_id, cmdb_comp_env.env, cmdb_comp_env.app_server,
			cmdb_comp_env.as_version, cmdb_comp_env.clustered, cmdb_comp_env.high_avail,
			cmdb_comp_env.deploy_location, cmdb_comp_env.vp_id, cmdb_comp_env.state
		FROM    cmdb_comp_gen, cmdb_comp_env, cmdb_server
		WHERE   cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id
		AND     cmdb_comp_env.server_id=cmdb_server.server_id
		AND     cmdb_server.server_name = ? and cmdb_server.env = ?
		ORDER BY cmdb_comp_gen.comp_name
")
    or die "Error preparing getCompEnv statement: $DBI::errstr\n";

my $getServerName = $dbh->prepare("
		  SELECT  cmdb_server.server_name
		  FROM    cmdb_server
		  WHERE   cmdb_server.server_id = ?
")
    or die "Error preparing getServerName statement: $DBI::errstr\n";

# Select vip and port from VIP_Port based on VP_ID from Comp_Env

my $getVip = $dbh->prepare("
	  SELECT  cmdb_vip_port.vip, cmdb_vip_port.port
	  FROM    cmdb_vip_port
	  WHERE   cmdb_vip_port.vp_id = ?
")
    or die "Error preparing getVip statement: $DBI::errstr\n";

use strict;
#use warnings;          # Turned this off to avoid warnings about unitialized variables for NULL columns
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my $count = "";
my $server = "";
my $compName = "";
my $compId = "";
my $compType = "";
my $systemId = "";
my $compEnvId = "";
my $version = "";
my $serverId = "";
my $serverName = "";
my $env = "";
my $appServer = "";
my $asVersion = "";
my $clustered = "";
my $highAvail = "";
my $deployLocation = "";
my $vpId = "";
my $vip = "";
my $port = "";
my $state = "";


###############################################
# Main                                        #
###############################################

# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What components are on server: ";
    print $q->textfield(-name => "se");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();

# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

    # Gather and output the list of current unique Language Version pairs

    print "Current list of Server Environment pairs:";
    print $q->br;

    $getServers->execute();
    while (($server, $env) = $getServers->fetchrow()) {
	print $q->br;
	print "$server $env";
    }
    $getServers->finish;

} else {                                # Second time thru - get info for the specified Server Env

    my $serverenvironment = uc($q->param('se'));
    # Parse the Server Environment string
    $_ = $serverenvironment;
    $env = $1 if (s/ (\S+)$//);
    $server = $_;

    # Verify Server Environment pair is valid
    $getServer->execute($server, $env);
    $count = $getServer->fetchrow();
    $getServer->finish;

    if ($count == 0) {
	 print "Server $server Environment $env not found\n";
	 print "Processing stopped\n";
	 exit 1;
    }

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>What components are on $server $env</title></head>";

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";

    # Output table headers line
    print "<TR><TH> COMP_NAME </TH><TH> COMP_ID </TH><TH> COMP_TYPE </TH><TH> SYSTEM_ID </TH><TH> COM_ENV_ID </TH><TH> ENV </TH><TH> VERSION </TH><TH> SERVER_ID </TH><TH> SERVER_NAME </TH><TH> APP_SERVER </TH><TH> AS_VERSION </TH><TH> CLUSTERED </TH><TH> HA </TH><TH> DEPLOY_LOCATION </TH><TH> VP_ID </TH><TH> VIP </TH><TH> PORT </TH><TH> STATE </TH></TR>";

    $getCompEnv->execute($server, $env);

    while (($compName, $compId, $compType, $systemId, $compEnvId, $version, $serverId, $env, $appServer, $asVersion, $clustered, $highAvail, $deployLocation, $vpId, $state) = $getCompEnv->fetchrow()) {

	    # Get Server Name
	    $getServerName->execute($serverId);
	    ($serverName) = $getServerName->fetchrow();
	    $getServerName->finish;

	    # Get VIP and PORT pairings
	    $getVip->execute($vpId);
	    ($vip, $port) = $getVip->fetchrow();
	    $getVip->finish;

	    print "<TR><TD> $compName </TD>";
	    print "<TD> $compId </TD>";
	    print "<TD> $compType </TD>";
	    print "<TD> $systemId </TD>";
	    print "<TD> $compEnvId </TD>";
	    print "<TD> $env </TD>";
	    print "<TD> $version </TD>";
	    print "<TD> $serverId </TD>";
	    print "<TD> $serverName </TD>";
	    print "<TD> $appServer </TD>";
	    print "<TD> $asVersion </TD>";
	    print "<TD> $clustered </TD>";
	    print "<TD> $highAvail </TD>";
	    print "<TD> $deployLocation </TD>";
	    print "<TD> $vpId </TD>";
	    print "<TD> $vip </TD>";
	    print "<TD> $port </TD>";
	    print "<TD> $state </TD>";

	    # Clear variables for next pass
	    $compName = "";
	    $compId = "";
	    $compType = "";
	    $compEnvId = "";
	    $version = "";
	    $serverId = "";
	    $serverName = "";
	    $env = "";
	    $appServer = "";
	    $asVersion = "";
	    $clustered = "";
	    $highAvail = "";
	    $deployLocation = "";
	    $vpId = "";
	    $vip = "";
	    $port = "";
	    $state = "";
    }

    $getCompEnv->finish;

}


###############################################
# Closing                                     #
###############################################

#$dbh->commit();


$dbh->disconnect;

exit 0;

