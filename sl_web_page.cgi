#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: sl_web_page.cgi                                                  #
#         This perl script generates the html for a web page               #
#         and populates the cells of the table with current Service        #
#         Locator info                                                     #
# Author: Herb Gregory                                                     #
# Date: August 3, 2011                                                     #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

###############################################
# Parse Command Line These lines could be uncommented to allow input of Oracle DB  #
###############################################

#if ($#ARGV == 0) {
#        $db = $ARGV[0];
#}
#else {
#        usage();
#}
$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

#$dbh = DBI->connect( $oraId, $login, $pass );
$dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Find ECS or ECS-CLUSTER DomainArchive for the specified environment.  Find the DomainArchive in
# comp_gen table, then find the comp_env_id, HighAvailability, Server ID, and VIP/PORT ID of the
# DomainArchive in the specified environment.

$findDA = $dbh->prepare("
		SELECT cmdb_comp_env.comp_env_id, cmdb_comp_env.high_avail, cmdb_comp_env.server_id, cmdb_comp_env.vp_id
		FROM cmdb_comp_env, cmdb_comp_gen
		WHERE cmdb_comp_gen.comp_name like 'ECS%'
		AND cmdb_comp_gen.comp_type = 'DomainArchive'
		AND cmdb_comp_env.comp_id = cmdb_comp_gen.comp_id
		AND cmdb_comp_env.env = ?
")
    or die "Error preparing findDA statement: $DBI::errstr\n";

# Get the VIP, Port for the VP_ID retrieved for the ECS-CLUSTER DomainArchive

$findDAVP = $dbh->prepare("
		SELECT cmdb_vip_port.vip, cmdb_vip_port.port
		FROM cmdb_vip_port
		WHERE cmdb_vip_port.vp_id = ?
")
    or die "Error preparing findDAVP statement: $DBI::errstr\n";

# Find F5 for the specified environment, unless PROD
# Get the VIP, Port for that F5
# NOTE: Use of "like '%CAS.ORG%' should be ok because these F5 VIPs must be retained for the long
# haul since they have been propagated to places like CDS and in DCTM

$findF5VP = $dbh->prepare("
		SELECT cmdb_vip_port.vip, cmdb_vip_port.port
		FROM cmdb_vip_port, cmdb_server
		WHERE cmdb_server.server_name like 'F5%'
		AND cmdb_server.env = ?
		AND cmdb_vip_port.server_id = cmdb_server.server_id
		AND cmdb_vip_port.vip like 'ECS%'
")
    or die "Error preparing findF5VP statement: $DBI::errstr\n";

# Find F5 for the PROD environment
# Get the VIP, Port for that F5
# NOTE: Use of "like '%CAS.ORG%' is needed here because the this VIP must be retained for the long
# haul since it has been propagated to places like CDS and in DCTM

$findPRODF5VP = $dbh->prepare("
		SELECT cmdb_vip_port.vip, cmdb_vip_port.port
		FROM cmdb_vip_port, cmdb_server
		WHERE cmdb_server.server_name like 'F5%'
		AND cmdb_server.env = ?
		AND cmdb_vip_port.server_id = cmdb_server.server_id
		AND cmdb_vip_port.vip like '%CAS.ORG%'
")
    or die "Error preparing findPRODF5VP statement: $DBI::errstr\n";


# Find the Jboss version for the ECS/ECS-CLUSTER domain application server

$findASVersion = $dbh->prepare("
		SELECT cmdb_comp_env.as_version
		FROM cmdb_comp_env
		WHERE cmdb_comp_env.comp_env_id = ?
")
    or die "Error preparing findASVersion statement: $DBI::errstr\n";

# Find Language Version for the Service Locator for the specified environment.  Find Service Locator
# in comp_gen table, then find the instances of Service Locator in the specified environment in the
# comp_env table, then find the language version in the language table.

$findLanguageVersion = $dbh->prepare("
		SELECT cmdb_language.version
		FROM cmdb_language, cmdb_comp_env, cmdb_comp_gen
		WHERE cmdb_comp_gen.comp_name = 'ServiceLocator'
		AND cmdb_comp_env.comp_id = cmdb_comp_gen.comp_id
		AND cmdb_comp_env.env = ?
		AND cmdb_language.comp_env_id = cmdb_comp_env.comp_env_id
")
    or die "Error preparing findLanguageVersion statement: $DBI::errstr\n";

# Get hosts (what is hosted in this environment) info from cmdb_env_hosts table

$findHosts = $dbh->prepare("
		SELECT cmdb_env_hosts.hosts
		FROM cmdb_env_hosts
		WHERE cmdb_env_hosts.env = ?
")
    or die "Error preparing findHosts statement: $DBI::errstr\n";

# Get server from cmdb_server table

$findServer = $dbh->prepare("
		SELECT cmdb_server.server_name
		FROM cmdb_server
		WHERE cmdb_server.server_id = ?
")
    or die "Error preparing findHosts statement: $DBI::errstr\n";


# Environments to step through

@envs = (
"SB01",
"DEV01",
"DEV02",
"DEV03",
"DEV04",
"QA01",
"QA02",
"QA03",
"QA04",
"PREP01",
"PREP02",
"PROD");


###############################################
# Main                                        #
###############################################

# Output CGI header line

print "Content-type: text/html\n\n";

# Output header line

print "<html><head><title>Sevice Locator Instances Hosted by ESOps Web Page</title></head>";       # This provides the title for display in the browser tool bar

# Capture date for outputting with second line of page and output second line of page
@date = localtime(time());
$year = 1900 + @date[5];
$date_string = ++@date[4]."/".@date[3]."/".$year;
print "Service Locator Instances Hosted by ESOps<body><p> as of $date_string</p>";

# Output table definition line
print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";

# Output table headers line
print "<TR><TH> ESOps Env </TH><TH> Use </TH><TH> Service Locator </TH><TH> Linux/Solaris Server </TH><TH> Service Locator VIP </TH><TH>  JBoss Ver. </TH><TH> JDK </TH><TH>  F5 Load Balancer VIP </TH></TR>\n";

# While more environments
$env_index = 0;
$env = @envs[$env_index];
while ($env ne "") {

    # Get Environment Hosts info
    $findHosts->execute($env);
    ($hosts) =  $findHosts->fetchrow();
    $findHosts->finish;

    # Find the ECS* Domain Archive (Jboss domain)
    $findDA->execute($env);
    ($comp_env_id, $ha, $server_id, $vp_id) =  $findDA->fetchrow();

    if ($ha eq 'N') {
	$findDA->finish;

	# Get Server Name for Domain Archive (Jboss domain)
	$findServer->execute($server_id);
	($server_name) =  $findServer->fetchrow();
	$findServer->finish;

	# Get VIP and Port
	$findDAVP->execute($vp_id);
	($vip, $port) =  $findDAVP->fetchrow();
	$findDAVP->finish;

	# Get Jboss (app server) Version
	$findASVersion->execute($comp_env_id);
	($as_version) =  $findASVersion->fetchrow();
	$findASVersion->finish;

	# Get Language Version (this will only retrieve info for the Prod environment)
	$findLanguageVersion->execute($env);
	($language_version) =  $findLanguageVersion->fetchrow();
	$findLanguageVersion->finish;

	if ($language_version eq "") {                  # Non-Prod environments only have DomainArchives
	    $language_version = "NA";                   # so they won't find a Language Table entry
	}                                               # Set Language Version to NA

	# Output web page table row
	print "<TD> $env </TD>";
	print "<TD> $hosts </TD>";
	print "<TD> <A CLASS=\"external\" HREF=\"http://$vip:$port/servicelocator\">http://$vip:$port/servicelocator </TD>";
	print "<TD> $server_name </TD>";
	print "<TD> <A CLASS=\"external\" HREF=\"http://$vip:$port\">http://$vip:$port </TD>";
	print "<TD> $as_version </TD>";
	print "<TD> $language_version </TD>";
	print "<TD> NA </TD></TR>\n";         # Since this is NOT HA, there is no F5

     } else {                           # THIS IS AN HA ENVIRONMENT


	if ($env ne "PROD") {
	    # Get F5 VIP and Port
	    $findF5VP->execute($env);
	    ($f5vip, $f5port) =  $findF5VP->fetchrow();
	    $findF5VP->finish;
	} else {
	    # Get F5 VIP and Port for PROD
	    $findPRODF5VP->execute($env);
	    ($f5vip, $f5port) =  $findPRODF5VP->fetchrow();
	    $findPRODF5VP->finish;
	}

	# Get Server Name for first side of HA
	$findServer->execute($server_id);
	($server_name1) =  $findServer->fetchrow();
	$findServer->finish;

	# Get VIP and Port for first side of HA
	$findDAVP->execute($vp_id);
	($vip1, $port1) =  $findDAVP->fetchrow();
	$findDAVP->finish;

	# Get Jboss (app server) Version - JUST GET THIS ONCE, ASSUMING BOTH SIDES OF HA ARE SAME VERSION
	$findASVersion->execute($comp_env_id);
	($as_version) =  $findASVersion->fetchrow();
	$findASVersion->finish;

	# Get Language Version - JUST GET THIS ONCE, ASSUMING BOTH SIDES OF HA ARE SAME VERSION
	$findLanguageVersion->execute($env);
	($language_version) =  $findLanguageVersion->fetchrow();
	$findLanguageVersion->finish;

	if ($language_version eq "") {                  # Non-Prod environments only have DomainArchives
	    $language_version = "NA";                   # so they won't find a Language Table entry
	}                                               # Set Language Version to NA

	# Find the ECS* Domain Archive (Jboss domain) for second side of HA
	($comp_env_id, $ha, $server_id, $vp_id) =  $findDA->fetchrow();
	$findDA->finish;

	# Get Server Name for second side of HA
	$findServer->execute($server_id);
	($server_name2) =  $findServer->fetchrow();
	$findServer->finish;

	# Get VIP and Port for second side of HA
	$findDAVP->execute($vp_id);
	($vip2, $port2) =  $findDAVP->fetchrow();
	$findDAVP->finish;

	# Output web page table row
	print "<TR><TD> $env </TD>";
	print "<TD> $hosts </TD>";
	if ($f5vip eq "") {
	    print "<TD> NA </TD>";
	} else {
	    print "<TD> <A CLASS=\"external\" HREF=\"http://$f5vip:$f5port/servicelocator\">http://$f5vip:$f5port/servicelocator </TD>";
	}
	print "<TD> $server_name1, $server_name2 </TD>";
	print "<TD> <A CLASS=\"external\" HREF=\"http://$vip1:$port1\">http://$vip1:$port1 <A CLASS=\"external\" HREF=\"http://$vip2:$port2\">http://$vip2:$port2 </TD>";
	print "<TD> $as_version </TD>";
	print "<TD> $language_version </TD>";
	if ($f5vip eq "") {
	    print "<TD> NA </TD></TR>\n";
	} else {
	    print "<TD> <A CLASS=\"external\" HREF=\"http://$f5vip:$f5port\">http://$f5vip:$f5port </TD></TR>\n";
	}

     }

   $env_index++;
   $env = @envs[$env_index];

   # Clear variables for next environment
   $hosts = "";
   $comp_env_id = "";
   $ha = "";
   $server_id = "";
   $vp_id = "";
   $server_name = "";
   $vip = "";
   $port = "";
   $as_version = "";
   $vip1 = "";
   $vip2 = "";
   $port1 = "";
   $port2 = "";
   $server_name = "";
   $server_name1 = "";
   $server_name2 = "";
   $language_version = "";

}

print "</TABLE></body></html>";

###############################################
# Closing                                     #
###############################################

$dbh->commit();

$dbh->disconnect;

exit 0;

###############################################
# Subroutines                                 #
###############################################

sub usage {
	$msg = $_[0];

	print "sl_web_page.pl: $msg\n";
	print "Usage: DB instance required input\n";
	print "       sl_web_page.pl P067\n";

	exit 1;
}
