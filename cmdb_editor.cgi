#!/usr/local/perl5/bin/perl

use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

print $q->header();

print "

<style type=\"text/css\">
<!--
body, {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}
-->
</style>
";

print $q->start_html(-title=>'CMDB Editor Index');

print "
<p>
<b>
CMDB Table Editor
</b>
</p>
<p>
Select table you wish to edit:
</p>

<p><h><b>PRODUCTION</b></h></p>

&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_env.cgi\">
CMDB_COMP_ENV
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_gen.cgi\">
CMDB_COMP_GEN
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_uses.cgi\">
CMDB_COMP_USES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_db.cgi\">
CMDB_DB
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_db_uses.cgi\">
CMDB_DB_USES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_env_hosts.cgi\">
CMDB_ENV_HOSTS
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_uses.cgi?startas=includes\">
CMDB_INCLUDES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_language.cgi\">
CMDB_LANGUAGE
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_server.cgi\">
CMDB_SERVER
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_vip_port.cgi\">
CMDB_VIP_PORT
</a>
<br>


<p><h><b>DEVELOPMENT</b></h></p>

&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_env-d.cgi\">
CMDB_COMP_ENV
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_gen-d.cgi\">
CMDB_COMP_GEN
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_uses-d.cgi\">
CMDB_COMP_USES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_db-d.cgi\">
CMDB_DB
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_db_uses-d.cgi\">
CMDB_DB_USES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_env_hosts-d.cgi\">
CMDB_ENV_HOSTS
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_comp_uses-d.cgi?startas=includes\">
CMDB_INCLUDES
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_language-d.cgi\">
CMDB_LANGUAGE
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_server-d.cgi\">
CMDB_SERVER
</a>
<br>
&nbsp;&nbsp;
<a href=\"http://homepage/projects/esops/www/cgi-bin/edit_vip_port-d.cgi\">
CMDB_VIP_PORT
</a>
<br>
&nbsp;&nbsp;

";

print $q->end_html();
