#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Create out headers and the Javascript functions
# we'll need.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillForm(server_id, comp_name, comp_type, system_id) {
	document.editFields.comp_id.value = server_id;
	document.editFields.name.value = comp_name;
	document.editFields.type[0].selected = true;
	document.editFields.type.value = comp_type;
	document.editFields.system_id.value = system_id;
}
function clearForm() {
	document.editFields.comp_id.value = '';
	document.editFields.type[0].selected = true;
	document.editFields.name.value = '';
	document.editFields.system_id.value = '';
}
function confirmDelete() {
        var answer = confirm(\"Delete this component record?\");
        if (answer){
                document.editFields.inputtype.value = \"Delete\";
                document.editFields.DELETE.value = \"yes\";
                document.editFields.submit();
        }
}
\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_COMP_GEN');

print "<b>Edit Component Gen Table</b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print $q->table(
	$q->Tr(
	[
		$q->td(["Name:", $q->textfield(-name=>"name", -size=>50)]),
		$q->td(["Type:", $q->popup_menu(-name=>"type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]),
		$q->td(["System ID:", $q->textfield(-name=>"system_id")]),
		$q->td(["Comp ID:", $q->textfield(-name=>"comp_id")])
	])
);
print "\n<input type=\"button\" onClick=\"clearForm();\" value=\"Clear Fields\" /></input><br>\n";
print $q->submit(-value => "Search", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Update", -name => "inputtype");
print "&nbsp;";
print $q->submit(-value => "Create New", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Delete\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

#
# If $q->param() equals "", then it's either the
# first time through, or something similar.
#
if ($inputtype eq 'Search') {
	my $name = $q->param('name');
	my $type = $q->param('type');
	my $id = $q->param('system_id');
	my $comp_id = $q->param('comp_id');
	#
	# Build while string
	#
	if(length($type) > 0) {
		$search_string = "comp_type='$type'";
	} else {
		$search_string = "comp_type like '%'";
	}
	if(length($name) > 0) {
		#
		# Tell oracle to translate to all lower
		# case so that we're doing a case insensitive
		# search.
		#
		$name =~ s/ /%/g;
		$search_string = $search_string . " and lower(comp_name) like lower('%$name%')"
	}
	if(length($id) > 0) {
		$search_string = $search_string . " and lower(system_id) like lower('%$id%')";
	}
#print "<br>select * from cmdb_comp_gen where $search_string order by comp_name<br>\n";
        $getTbl = $dbh->prepare("select * from cmdb_comp_gen where $search_string order by comp_name") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Comp ID</th>\n";
	print "<th>Comp Name</th>\n";
	print "<th>Comp Type</th>\n";
	print "<th>System ID</th>\n";
	print "</tr>\n";
	while (($comp_id, $name, $type, $id) = $getTbl->fetchrow()) {
		if(length($name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillForm($comp_id, '$name', '$type', '$id')\">$comp_id</a></td>\n";
			print "<td>$name</td>\n";
			print "<td>$type</td>\n";
			#
			# If you put an data element in with no value,
			# ie. "<td></td>" it messes up the way the 
			# table looks, so if the os length is zero,
			# put a non-breakable space in to make a real
			# table cell.
			#
			if(length($id) > 0) {
				print "<td>$id</td>\n";
			} else {
 				print "<td>&nbsp</td>\n";
			}
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
	

} elsif ($inputtype eq 'Create New') {
	my $name = $q->param('name');
	my $type = $q->param('type');
	my $id = $q->param('system_id');

	if(length($name) < 1 || length($type) < 1 || length($id) < 1) {
                print "<p><b>Error: All fields must have values</b></p>\n";
                print $q->end_html();
                #$dbh->commit();
                $dbh->disconnect;
                exit;
        }

	$insstr = "select comp_id from cmdb_comp_gen where comp_name='$name' and comp_type='$type'";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while ($temp = $getTbl->fetchrow()) {
		$i++;
	}
	$getTbl->finish;
	if($i != 0) {
		print "<p><b>Duplicate entry detected.  No change made to table.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "insert into cmdb_comp_gen (comp_name, comp_type, system_id) values ('$name', '$type', '$id')";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_COMP_GEN completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_COMP_GEN failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Update') {
	my $name = $q->param('name');
	my $type = $q->param('type');
	my $id = $q->param('system_id');
	my $comp_id = $q->param('comp_id');
	if($comp_id < 1) {
		print"<p><b>Comp ID is zero.  Perhaps you meant to create a new record?</b></p>\n";
	} else {
		$insstr = "update cmdb_comp_gen set comp_name='$name', comp_type='$type', system_id='$id' where comp_id=$comp_id";

		#print "<br>$insstr<br>\n";

		$getTbl = $dbh->prepare($insstr);
		$i = $getTbl->execute();
		$getTbl->finish;
		if($i != 0) {
			print "<p>Update to CMDB_COMP_GEN completed.</p>\n";
		} else {
			print "<p>Update to CMDB_COMP_GEN failed:</p>\n";
			print "<p>$insstr</p>\n";
		}
	}

} elsif ($inputtype eq 'Delete' || $do_delete eq 'yes') {
        my $comp_id = $q->param('comp_id');
        if($comp_id < 1 || $comp_id == "") {
                print"<p><b>COMP_ID is zero.  Can't delete that.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

	$insstr = "select comp_env_id from cmdb_comp_env where comp_id=$comp_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$i = 0;
	while ($temp = $getTbl->fetchrow()) {
		$i++;
	}
	$getTbl->finish;
	if($i != 0) {
		print "<p><b>This COMP_ID exists in the CMDB_COMP_ENV table.  That record must be deleted before removing this record.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

        $insstr = "delete from cmdb_comp_gen where comp_id=$comp_id";

        #print "<br>$insstr<br>\n";

        $getTbl = $dbh->prepare($insstr);
        $i = $getTbl->execute();
        $getTbl->finish;
        if($i != 0) {
                print "<p>Delete from CMDB_COMP_GEN completed.</p>\n";
        } else {
                print "<p>Delete from CMDB_COMP_GEN failed:</p>\n";
                print "<p>$insstr</p>\n";
        }
}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


