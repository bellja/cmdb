#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: domain-all-info.cgi                                              #
# Author: Herb Gregory                                                     #
# Date: Aug 17, 2011                                                       #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get Env and Hosts infor for all environments

my $getEnvHosts = $dbh->prepare("
		SELECT  cmdb_env_hosts.env, cmdb_env_hosts.hosts
		FROM    cmdb_env_hosts
		ORDER BY env
")
    or die "Error preparing getEnvHosts statement: $DBI::errstr\n";

# Get Host info for the specified Environment

my $getHost = $dbh->prepare("
		  SELECT  cmdb_env_hosts.hosts
		  FROM    cmdb_env_hosts
		  WHERE   cmdb_env_hosts.env = ?
")
    or die "Error preparing getHost statement: $DBI::errstr\n";

# Get F5 for the specified Environment

my $getF5 = $dbh->prepare("
		  SELECT  cmdb_server.server_name, cmdb_server.server_id, cmdb_server.os
		  FROM    cmdb_server
		  WHERE   cmdb_server.server_name like 'F5%' and cmdb_server.env = ?
		  ORDER BY os
")
    or die "Error preparing getF5 statement: $DBI::errstr\n";

# Get VIP and Port for the F5 and Environment

my $getF5Vip = $dbh->prepare("
	  SELECT  cmdb_vip_port.vip, cmdb_vip_port.port
	  FROM    cmdb_vip_port
	  WHERE   cmdb_vip_port.server_id = ?
	  ORDER BY cmdb_vip_port.vip
")
    or die "Error preparing getF5Vip statement: $DBI::errstr\n";

# Get Server Name and Server ID and Server OS for the specified Environment

my $getServerName = $dbh->prepare("
		  SELECT  cmdb_server.server_name, cmdb_server.server_id, cmdb_server.os
		  FROM    cmdb_server
		  WHERE   cmdb_server.env = ?
		  ORDER BY os, server_name
")
    or die "Error preparing getServerName statement: $DBI::errstr\n";

# Get the count of Components associated with a specific Server ID

my $getCompCount = $dbh->prepare("
		  SELECT  count(*)
		  FROM    cmdb_comp_env
		  WHERE   cmdb_comp_env.server_id = ?
")
    or die "Error preparing getCompCount statement: $DBI::errstr\n";

# Get Deployment Location, vip/port ID, State, Component Name and Componenet Type for the component on the specified Server

my $getComp = $dbh->prepare("
		  SELECT  cmdb_comp_env.deploy_location, cmdb_comp_env.vp_id, cmdb_comp_env.state, cmdb_comp_gen.comp_name, cmdb_comp_gen.comp_type
		  FROM    cmdb_comp_env, cmdb_comp_gen
		  WHERE   cmdb_comp_env.server_id = ?
		  AND     cmdb_comp_gen.comp_id = cmdb_comp_env.comp_id
		  ORDER BY cmdb_comp_gen.comp_name
")
    or die "Error preparing getComp statement: $DBI::errstr\n";


# Get VIP and Port

my $getVip = $dbh->prepare("
	  SELECT  cmdb_vip_port.vip, cmdb_vip_port.port
	  FROM    cmdb_vip_port
	  WHERE   cmdb_vip_port.vp_id = ?
")
    or die "Error preparing getVip statement: $DBI::errstr\n";

my $getDBCount = $dbh->prepare("
		  SELECT  count(*)
		  FROM    cmdb_db
		  WHERE   cmdb_db.server_id = ?
")
    or die "Error preparing getDBCount statement: $DBI::errstr\n";

# Get Narrative Name, DB Name, Type, Schema, Path  and State for the DB

my $getDB = $dbh->prepare("
		  SELECT  narrative_name, db_name, db_type, schema, path, state
		  FROM    cmdb_db
		  WHERE   cmdb_db.server_id = ?
		  ORDER BY cmdb_db.narrative_name
")
    or die "Error preparing getDB statement: $DBI::errstr\n";

use strict;
#use warnings;          # Turned this off to avoid warnings about unitialized variables for NULL columns
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my $env         = "";
my $hosts       = "";
my $serverName  = "";
my $serverId    = "";
my $os = "";
my $count = "";
my $compName = "";
my $compType = "";
my $deployLocation = "";
my $vpId = "";
my $vip = "";
my $port = "";
my $state = "";
my $narrativeName = "";
my $dbName = "";
my $type = "";
my $schema = "";
my $path = "";
my $state = "";

###############################################
# Main                                        #
###############################################


# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What environment do you want information for: ";
    print $q->textfield(-name => "env");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();

# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

    # Gather and output the list of current Environments and what they are Hosting

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>Environment Query</title></head>";

    print "Current list of Environments:";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> ENVIRONMENT </TH><TH> HOSTS </TH></TR>";

    $getEnvHosts->execute();
    while (($env, $hosts) = $getEnvHosts->fetchrow()) {
       # Output one web page row
	print "<TR><TD> $env </TD>";
	print "<TD> $hosts </TD></TR>";

	# Clear variables for next pass
	$env = "";
	$hosts = "";

    }# Process Environment info for the specified Environment
    $getEnvHosts->finish;

} else {                          # Second time thru - get info for the specified environment

    my $env = uc($q->param('env'));

    ########## Output Environment and Hosts info ##########

    $getHost->execute($env);
    ($hosts) = $getHost->fetchrow();
    $getHost->finish;

    if ($hosts eq "") {
	    print "Environment $env not found in cmdb_env_host table\n";
	    print "Processing stopped\n";
	    exit 1;
    }


    # Output the browser header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>Information for ESOps environment $env</title></head>";

    print "ENVIRONMENT HOST INFORMATION";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> ENVIRONMENT </TH><TH> HOSTS </TH></TR>";

    print "<TR><TD> $env </TD>";
    print "<TD> $hosts </TD></TR>";
    print "</TABLE CLASS>";
    print $q->br;               # Blank line between tables

    ########## Process F5 Router info for the specified Environment ##########

    print "F5 INFO";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> F5 </TH><TH> OS </TH><TH> VIP</TH><TH> PORT </TH></TR>";

    $getF5->execute($env);

    if (($serverName, $serverId, $os) = $getF5->fetchrow()) {

	    # Get the VIP and PORT combinations for the F5 for this Environment
	    $getF5Vip->execute($serverId);
	    while (($vip, $port) = $getF5Vip->fetchrow()) {

		print "<TR><TD> $serverName </TD>";
		print "<TD> $os </TD>";
		print "<TD> $vip </TD>";
		print "<TD> $port </TD></TR>";

		$vip = "";
		$port = "";

	    }
	    $getF5Vip->finish;
    }
    print "</TABLE CLASS>";
    $getF5->finish;
    print $q->br;

    ######### Process Servers in  OS order: Linux, Solaris then Windows #########
    # Only output info for servers that have Components associated with them
    # This output should simulate the output requested to populate a web page

    print "COMPONENT INFO";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> SERVER </TH><TH> OS </TH><TH> COMP_NAME</TH><TH> COMP_TYPE </TH><TH> DEPLOY_LOCATION</TH><TH> VIP </TH><TH> PORT </TH><TH> STATE </TH></TR>";

    $getServerName->execute($env);

    while (($serverName, $serverId, $os) = $getServerName->fetchrow()) {

	    # See if there are any Components associated with this server
	    $getCompCount->execute($serverId);
	    ($count) = $getCompCount->fetchrow();
	    $getCompCount->finish;

	    if ($count > 0) {
		# Loop through Components

		# Get Component deployment location, vp_id, name and type
		$getComp->execute($serverId);

		while (($deployLocation, $vpId, $state, $compName, $compType) = $getComp->fetchrow()) {
		    # Get the VIP and PORT for this Component
		    $getVip->execute($vpId);
		    ($vip, $port) = $getVip->fetchrow();
		    $getVip->finish;

		    print "<TR><TD> $serverName </TD>";
		    print "<TD> $os </TD>";
		    print "<TD> $compName </TD>";
		    print "<TD> $compType </TD>";
		    print "<TD> $deployLocation </TD>";
		    print "<TD> $vip </TD>";
		    print "<TD> $port </TD>";
		    print "<TD> $state </TD></TR>";
		    $compName = "";
		    $compType = "";
		    $deployLocation = "";
		    $vip = "";
		    $port = "";
		    $state = "";

		}
		$getComp->finish;
	    }
    }
    print "</TABLE CLASS>";
    print $q->br;
    $getServerName->finish;

    ######### REPEAT ABOVE STEPS FOR DATABASES ###########

    # Process Servers in  OS order: Linux, Solaris then Windows
    # Only output info for servers that have Databases associated with them
    # This output should simulate the output requested to populate a web page

    print " DATABASE INFO\n";
    print $q->br;

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";
    # Output table headers line
    print "<TR><TH> SERVER </TH><TH> OS </TH><TH> NARRATIVE_NAME</TH><TH> DB_NAME </TH><TH> TYPE</TH><TH> SCHEMA </TH><TH> PATH </TH><TH> STATE </TH></TR>";

    $getServerName->execute($env);

    while (($serverName, $serverId, $os) = $getServerName->fetchrow()) {

	    # See if there are any Databases associated with this server
	    $getDBCount->execute($serverId);
	    ($count) = $getDBCount->fetchrow();
	    $getDBCount->finish;

	    if ($count > 0) {
		# Loop through DBs

		# Get narrative name, DB name, type and schema
		$getDB->execute($serverId);

		while (($narrativeName, $dbName, $type, $schema, $path, $state) = $getDB->fetchrow()) {
		    print "<TR><TD> $serverName </TD>";
		    print "<TD> $os </TD>";
		    print "<TD> $narrativeName </TD>";
		    print "<TD> $dbName </TD>";
		    print "<TD> $type </TD>";
		    print "<TD> $schema </TD>";
		    print "<TD> $path </TD>";
		    print "<TD> $state </TD></TR>";
		    $narrativeName = "";
		    $dbName = "";
		    $type = "";
		    $schema = "";
		    $path = "";
		    $state = "";

		}
		$getDB->finish;
	    }


    }
    $getServerName->finish;
    print "</TABLE CLASS>";
}


###############################################
# Closing                                     #
###############################################

#$dbh->commit();


$dbh->disconnect;

exit 0;

