#! /usr/local/perl5/bin/perl

############################################################################
#                        Script Comment Block                              #
#                                                                          #
# Script: what-uses-language-x.cgi                                         #
# Author: Herb Gregory                                                     #
# Date: Aug 16, 2011                                                       #
# CAS - Editorial Operations                                               #
#                                                                          #
############################################################################
############################################################################

# NOTE:
# Logs are at srv22:/usr2/iplanet/servers/web/prod/https-homepage/logs/errors

$ENV{SHELL} = "/bin/ksh";
use DotShell;

$db = "P067";

###############################################
# Init DBI, Prepare SQL Statements            #
###############################################

use DBI;

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;

my $dbh = DBI->connect( $oraId, "n390", "oxyops1" );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;

# Get unique Language, Version pairs

my $getAllLanguageVersion = $dbh->prepare("
		SELECT UNIQUE cmdb_language.language, cmdb_language.version
		FROM cmdb_language
")
    or die "Error preparing getAllLanguageVersion statement: $DBI::errstr\n";

# Get specific Language, Version pair

my $getLanguageVersion = $dbh->prepare("
		SELECT count(*)
		FROM cmdb_language
		WHERE cmdb_language.language = ? and cmdb_language.version = ?
")
    or die "Error preparing getLanguageVersion statement: $DBI::errstr\n";

# Get COMP_ENV_ID from Language table for the specified language version pair
my $getCompEnvId = $dbh->prepare("
		SELECT  cmdb_language.comp_env_id
		FROM    cmdb_language
		WHERE     cmdb_language.language = ? and version = ?
")
    or die "Error preparing getCompEnvId statement: $DBI::errstr\n";

# Get the Component ID from  COMP ENV table

my $getCompIdfromEnv = $dbh->prepare("
		SELECT cmdb_comp_env.comp_id
		FROM   cmdb_comp_env
		WHERE  cmdb_comp_env.comp_env_id = ?
")
    or die "Error preparing getCompIdfromEnv statement: $DBI::errstr\n";

# Get the Component General information from the COMP GEN table

my $getCompGen = $dbh->prepare("
		SELECT cmdb_comp_gen.comp_name, cmdb_comp_gen.comp_type, cmdb_comp_gen.system_id
		FROM   cmdb_comp_gen
		WHERE  cmdb_comp_gen.comp_id = ?
")
    or die "Error preparing getCompGen statement: $DBI::errstr\n";

# Get Component Environment information from the COMP ENV table

my $getCompEnv = $dbh->prepare("
		  SELECT cmdb_comp_env.vp_id, cmdb_comp_env.server_id, cmdb_comp_env.env,
			 cmdb_comp_env.version, cmdb_comp_env.app_server, cmdb_comp_env.as_version,
			 cmdb_comp_env.clustered, cmdb_comp_env.high_avail,
			 cmdb_comp_env.deploy_location, cmdb_comp_env.state
		  FROM   cmdb_comp_env
		  WHERE  cmdb_comp_env.comp_env_id = ?
")
    or die "Error preparing getCompEnv statement: $DBI::errstr\n";

# Select server name from Server based on Server_ID from Comp_Env

my $getServer = $dbh->prepare("
		SELECT cmdb_server.server_name
		FROM   cmdb_server
		WHERE  server_id = ?
")
    or die "Error preparing getServerstatement: $DBI::errstr\n";

# Select vip and port from VIP_Port based on Server_ID from Comp_Env

my $getVipPort = $dbh->prepare("
		 SELECT cmdb_vip_port.vip, cmdb_vip_port.port
		 FROM   cmdb_vip_port
		 WHERE  server_id = ?
")
    or die "Error preparing getVipPortstatement: $DBI::errstr\n";

use strict;
#use warnings;          # Turned this off to avoid warnings about unitialized variables for NULL columns
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $q = new CGI;

# Declare variables with "my" prefix to work within CGI
my  $lv = "";
my  $lang = "";
my  $ver = "";
my  $language = "";
my  $version = "";
my  $count = "";
my  $compName = "";
my  $compId = "";
my  $compType = "";
my  $systemId = "";
my  $compEnvId = "";
my  $compEnv = "";
my  $version = "";
my  $server_id = "";
my  $serverName = "";
my  $appServer = "";
my  $asVersion = "";
my  $clustered = "";
my  $ha = "";
my  $deployLocation = "";
my  $vpId = "";
my  $vip = "";
my  $port = "";
my  $state = "";

###############################################
# Main                                        #
###############################################

# This script is executed both to initially display the form to request which component is desired
# and again after the user enters a component.  Examining the $q->param() tells us which time this is

print $q->header();

# Output the form
    print $q->start_form();
    print "What uses language version: ";
    print $q->textfield(-name => "lv");
    print $q->submit(-value => "Click here");
    print $q->end_form();
    print $q->end_html();


# If $q->param() equals "", this is the first time through.
if ($q->param() eq "") {

    # Gather and output the list of current unique Language Version pairs

    print "Current list of Language Version pairs:";
    print $q->br;

    $getAllLanguageVersion->execute();
    while (($lang, $ver) = $getAllLanguageVersion->fetchrow()) {
	print $q->br;
	print "$lang $ver";
    }
    $getAllLanguageVersion->finish;

} else {                                # Second time thru - get info for the specified component
    my $languageversion = uc($q->param('lv'));
    # Parse the Language Version string
    $_ = $languageversion;
    $language = $1 if ( /(\S+) \S+/ );
    $version = $1 if ( /\S+ (\S+)/ );

    # Verify Language Version pair is valid
    $getLanguageVersion->execute($language, $version);
    $count = $getLanguageVersion->fetchrow();
    $getLanguageVersion->finish;

    if ($count == 0) {
	print "Language $language Version $version not found\n";
	print "Processing stopped\n";
	exit 1;
    }

    # Output header line
    # This provides the title for display in the browser tool bar
    print "<html><head><title>What uses $language $version</title></head>";

    # Output table definition line
    print "<TABLE CLASS=\"wikitable\" BORDER=\"1\">";

    # Output table headers line
    print "<TR><TH> COMP_NAME </TH><TH> COMP_ID </TH><TH> COMP_TYPE </TH><TH> SYSTEM_ID </TH><TH> COM_ENV_ID </TH><TH> ENV </TH><TH> VERSION </TH><TH> SERVER_ID </TH><TH> SERVER_NAME </TH><TH> APP_SERVER </TH><TH> AS_VERSION </TH><TH> CLUSTERED </TH><TH> HA </TH><TH> DEPLOY_LOCATION </TH><TH> VP_ID </TH><TH> VIP </TH><TH> PORT </TH><TH> STATE </TH></TR>";



    $getCompEnvId->execute($language, $version);

    # Process each Component
    while (($compEnvId) = $getCompEnvId->fetchrow()) {

	    # Get and output info for one Component
	    # Get COMP ID from COMP ENV table for the component
	    $getCompIdfromEnv->execute($compEnvId);
	    ($compId) =  $getCompIdfromEnv->fetchrow();
	    $getCompIdfromEnv->finish;

	    # Get COMP GEN info
	    $getCompGen->execute($compId);
	    ($compName, $compType, $systemId) =  $getCompGen->fetchrow();
	    $getCompGen->finish;

	    # Get COMP ENV info
	    $getCompEnv->execute($compEnvId);
	    ($vpId, $server_id, $compEnv, $version, $appServer, $asVersion, $clustered, $ha, $deployLocation, $state) =  $getCompEnv->fetchrow();
	    $getCompEnv->finish;

	    # Get Server Name
	    $getServer->execute($server_id);
	    $serverName =  $getServer->fetchrow();
	    $getServer->finish;

	    # Get VIP and PORT
	    $getVipPort->execute($vpId);
	    ($vip, $port) =  $getVipPort->fetchrow();
	    $getVipPort->finish;


	    print "<TR><TD> $compName </TD>";
	    print "<TD> $compId </TD>";
	    print "<TD> $compType </TD>";
	    print "<TD> $systemId </TD>";
	    print "<TD> $compEnvId </TD>";
	    print "<TD> $compEnv </TD>";
	    print "<TD> $version </TD>";
	    print "<TD> $server_id </TD>";
	    print "<TD> $serverName </TD>";
	    print "<TD> $appServer </TD>";
	    print "<TD> $asVersion </TD>";
	    print "<TD> $clustered </TD>";
	    print "<TD> $ha </TD>";
	    print "<TD> $deployLocation </TD>";
	    print "<TD> $vpId </TD>";
	    print "<TD> $vip </TD>";
	    print "<TD> $port </TD>";
	    print "<TD> $state </TD>";

	    # Clear variables for next pass
	    $compName = "";
	    $compId = "";
	    $compType = "";
	    $systemId = "";
	    $compEnvId = "";
	    $compEnv = "";
	    $version = "";
	    $server_id = "";
	    $serverName = "";
	    $appServer = "";
	    $asVersion = "";
	    $clustered = "";
	    $ha = "";
	    $deployLocation = "";
	    $vpId = "";
	    $vip = "";
	    $port = "";
	    $state = "";

    }
    $getCompEnvId->finish;

}

print "</TABLE></body></html>";

###############################################
# Closing                                     #
###############################################

#my $dbh->commit();

$dbh->disconnect;

exit 0;

