#!/usr/local/perl5/bin/perl

$ENV{SHELL} = "/bin/ksh";
use DotShell;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;

# For production
#$db = "P067";
# For dev
$db = "D067";
$Env = "QA02";

dotShell("/cas/bin/sun4/casora", "ORASID=$db");
$oraId = "dbi:Oracle:" . $db;
%attr = ( PrintError => 1, RaiseError => 1);

# For production
#$dbh = DBI->connect( $oraId, "n390", "oxyops1", \%attr);
# For dev
$dbh = DBI->connect( $oraId, "n390", "sn0wb0ard", \%attr);
#$dbh = DBI->connect( $oraId, $login, $pass );
if ( !defined $dbh ) {
    die "Cannot do \$dbh->connect: $DBI::errstr\n";
}
$dbh->{AutoCommit}  = 0;


my $q = new CGI;

#
# Create out headers and the Javascript functions
# we'll need.
#
print $q->header();
printf("\n<style type=\"text/css\">\n");
printf("<!--\n");
printf("body, table {font-family:\"Verdana,Arial,Helvetica,sans-serif\";color:\"black\";background-color:\"white\"; font-size:0.875em;}\n");
printf("-->\n");
printf("</style>\n\n");
print "<script type=\"text/javascript\">\n";
print "
function fillDBForm(db_name, narrative_name, db_type, schema, env, server_id, db_id) {
	document.editFields.db_name.value = '';
	document.editFields.narrative_name.value = '';
	document.editFields.db_type.value = '';
	document.editFields.schema.value = '';
	document.editFields.r_env[0].selected = true;
	document.editFields.db_server[0].selected = true;
	document.editFields.db_id.value = '';
	document.editFields.db_name.value = db_name;
	document.editFields.narrative_name.value = narrative_name;
	document.editFields.db_type.value = db_type;
	document.editFields.schema.value = schema;
	for(i = 0; i < document.editFields.r_env.length; i++) {
		if (document.editFields.r_env[i].value == env) {
			document.editFields.r_env[i].selected = true;
		}
	}
	for(i = 0; i < document.editFields.db_server.length; i++) {
		if (document.editFields.db_server[i].value == server_id) {
			document.editFields.db_server[i].selected = true;
		}
	}
	document.editFields.db_id.value = db_id;
	
}
function fillUsedForm(comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location) {
	document.editFields.l_name.value = '';
	document.editFields.l_type[0].selected = true;
	document.editFields.l_system_id.value = '';
	document.editFields.l_env[0].selected = true;
	document.editFields.l_server[0].selected = true;
	document.editFields.l_deploy_location.value = '';
	document.editFields.l_app_server.value = '';
	document.editFields.l_env_id.value = '';
	document.editFields.l_name.value = comp_name;
	for(i = 0; i < document.editFields.l_type.length; i++) {
		if (document.editFields.l_type[i].value == comp_type) {
			document.editFields.l_type[i].selected = true;
		}
	}
	document.editFields.l_system_id.value = system_id;
	for(i = 0; i < document.editFields.l_env.length; i++) {
		if (document.editFields.l_env[i].value == env) {
			document.editFields.l_env[i].selected = true;
		}
	}
	for(i = 0; i < document.editFields.l_server.length; i++) {
		if (document.editFields.l_server[i].value == server_id) {
			document.editFields.l_server[i].selected = true;
		}
	}
	document.editFields.l_app_server.value = app_server;
	document.editFields.l_env_id.value = comp_env_id;
	document.editFields.l_deploy_location.value = deploy_location;
}
function clearForm(side) {
	if(side == \"R\") {
		document.editFields.db_name.value = '';
		document.editFields.narrative_name.value = '';
		document.editFields.db_type.value = '';
		document.editFields.schema.value = '';
		document.editFields.r_env[0].selected = true;
		document.editFields.db_server[0].selected = true;
		document.editFields.db_id.value = '';
	} else {
		document.editFields.l_name.value = '';
		document.editFields.l_type[0].selected = true;
		document.editFields.l_system_id.value = '';
		document.editFields.l_env[0].selected = true;
		document.editFields.l_server[0].selected = true;
		document.editFields.l_deploy_location.value = '';
		document.editFields.l_app_server.value = '';
		document.editFields.l_env_id.value = '';
	}
}
function confirmDelete() {
        if(document.editFields.l_env_id.value == 0 || document.editFields.l_env_id.value == '' || document.editFields.db_id.value == 0 || document.editFields.db_id.value == '') {
                alert(\"A valid ENV ID or DB ID must exist.\");
	} else {
		var answer = confirm(\"Delete this environment record?\");
		if (answer){
			document.editFields.inputtype.value = \"Delete\";
			document.editFields.DELETE.value = \"yes\";
			document.editFields.submit();
		}
	}
}

\n";
print "</script>\n";"</script>\n";

print $q->start_html('Edit CMDB_DB_USES');

#
# Get a list of server names
#
$getTbl = $dbh->prepare("select unique server_name, server_id, env from cmdb_server order by server_name, env") or die "Error preparing
statement: $DBI::errstr\n";
$getTbl->execute();
$server[0]->{server_name} = "";
$server[0]->{server_id} = 0;
$server[0]->{env} = "";
$i = 1;
while (($server[$i]->{server_name}, $server[$i]->{server_id}, $server[$i]->{env}) = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

#
# Get the environments.
#
$getTbl = $dbh->prepare("select unique env from cmdb_env_hosts order by env") or die "Error preparing statemen
t: $DBI::errstr\n";
$getTbl->execute();
$i = 1;
$a_env[$i] = "";
while ($a_env[$i] = $getTbl->fetchrow()) {
        $i++;
}
$getTbl->finish;

print "<b>Edit DB Uses Table</b><br>\n";

%params = $q->Vars;

#print "<pre>\n";
#foreach $key (sort keys(%params)) {
#	print "$key = $params{$key}<p>";
#}
#print "</pre>\n";

#
# The value of the submit button that was
# clicked.  Either Search, Update, or Create New.
#
$inputtype = $q->param('inputtype');
$do_delete = $q->param('DELETE');

# Output the form
print "<p><a href=\"http://homepage/projects/esops/www/cgi-bin/cmdb_editor.cgi\">Return to Index Page</a></p>\n";
print $q->start_form(-name=>"editFields");
print "\n<input type=\"hidden\" name=\"DELETE\" value=\"no\" />\n";
print "<table border=1>\n";
print "<tr>\n";
print "<td>\n";


print "<table>\n";
print "<tr>\n";
print $q->td(["Name:", $q->textfield(-name=>"l_name", -size=>30)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Type:", $q->popup_menu(-name=>"l_type", -values=>["", "SoftwareComponent", "VendorArchive","DomainArchive","DataBaseline","Configuration","XMLCatalog","ClientLibrary"])]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["System ID:", $q->textfield(-name=>"l_system_id")]);
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Environment:\n";
print "</td>\n";
print "<td>\n";
print $q->popup_menu(-name=>"l_env", -values=>\@a_env);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Server:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='l_server' class=fixed>\n";
$s = 0;
$r_server = $q->param('l_server');
for($i = 0; $i <= $#server; $i++) {
        print "<option value=\"$server[$i]->{server_id}\"";
        if($r_server == $server[$i]->{server_id} && $s == 0) {
                print " selected";
                $s = 1;
        }
        print ">\n";
        if($i > 0 && length($server[$i]->{server_name}) > 0) {
                printf "%s", $server[$i]->{server_name};
        } else {
                print "&nbsp;"
        }
        print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["Deploy Location:", $q->textfield(-name=>"l_deploy_location", -size=>"55")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["App Server:", $q->textfield(-name=>"l_app_server")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Env ID:", $q->textfield(-name=>"l_env_id", -readonly=>"readonly")]);
print "</tr>\n";
print "<tr><td colspan=2>\n";
print "<table>\n";
print "<tr>\n";
print "<td>\n";
print "\n<input type=\"button\" onClick='clearForm(\"L\");' value=\"Clear Fields\" /></input>\n";
print "</td>\n";
print "<td>\n";
#print "&nbsp;";
print $q->submit(-value => "Search", -name => "l_search");
#print "<br>";
print "</tr><tr>\n";
print "<td colspan=2>\n";
print "Find items in CMDB_DB which are used by this Env ID.<br>\n";
print $q->submit(-value => "Find Used By", -name => "inputtype");
print "</td>\n";
print "</tr></table>\n";
print "</td></tr>\n";
print "</table>\n";


print "</td>\n";
print "<td>\n";
print "&nbsp;\n";
print "&nbsp;\n";
print "<b>uses -></b>\n";
print "&nbsp;\n";
print "&nbsp;\n";
print "</td>\n";
print "<td>\n";

print "<table>\n";
print "<tr>\n";
print $q->td(["DB Name:", $q->textfield(-name=>"db_name", -size=>30)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Narrative Name:", $q->textfield(-name=>"narrative_name", -size=>40)]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["DB Type:", $q->textfield(-name=>"db_type")]);
print "</tr>\n";
print "<tr>\n";
print $q->td(["Schema:", $q->textfield(-name=>"schema")]);
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Environment:\n";
print "</td>\n";
print "<td>\n";
print $q->popup_menu(-name=>"r_env", -values=>\@a_env);
print "</td>\n";
print "</tr>\n";
print "<tr>\n";
print "<td>\n";
print "Server:\n";
print "</td><td>\n";
#print "<select style=\"class:fixed\">\n";
print "<select name='db_server' class=fixed>\n";
#print "<select name='db_server_id'>\n";
$s = 0;
$r_server = $q->param('db_server');
for($i = 0; $i <= $#server; $i++) {
        print "<option value=\"$server[$i]->{server_id}\"";
        if($r_server == $server[$i]->{server_id} && $s == 0) {
                print " selected";
                $s = 1;
        }
        print ">\n";
        if($i > 0 && length($server[$i]->{server_name}) > 0) {
                printf "%s", $server[$i]->{server_name};
        } else {
                print "&nbsp;"
        }
        print "</option>\n";
}
print "</select>\n";
print "</td></tr>\n";
print "<tr>\n";
print $q->td(["DB ID:", $q->textfield(-name=>"db_id", -readonly=>"readonly")]);
print "</tr>\n";
print "<tr><td colspan=2>\n";
print "<table>\n";
print "<tr>\n";
print "<td>\n";
print "\n<input type=\"button\" onClick='clearForm(\"R\");' value=\"Clear Fields\" /></input>\n";
print "</td>\n";
print "<td>\n";
#print "&nbsp;";
print $q->submit(-value => "Search", -name => "r_search");
#print "<br>";
print "</td>\n";
print "</tr><tr>\n";
print "<td colspan=2>\n";
print "Find items in CMDB_DB which use this DB ID.<br>\n";
print $q->submit(-value => "Find Users", -name => "inputtype");
#print "&nbsp;";
print "</td>\n";
print "</tr>\n";
print "</table>\n";
print "</td>\n";
print "</tr>\n";
print "</table>\n";



print "</td></tr>\n";
print "</table>\n";
print "<br>\n";
print $q->submit(-value => "Create New Relationship", -name => "inputtype");
#print "&nbsp;";
#print $q->submit(-value => "Current Relationship", -name => "inputtype");
print "\n<input type=\"button\" onClick=\"confirmDelete();\" value=\"Remove Relationship\" name=\"inputtype\"/></input><br>\n";
print $q->end_form();

#
# Since we need to recognize which search was
# clicked, there are two possible submit string
# names for the search: l_search and r_search.  If 
# either one of those was defined, then we need
# to do a search.
#
if(defined($q->param('r_search'))) {
	my $db_name;
	my $db_type;
	my $narrative_name;
	my $schema;
	my $env;
	my $server_id;
	my $a = 0;
	$db_name = $q->param('db_name');
	$db_type = $q->param('db_type');
	$narrative_name = $q->param('narrative_name');
	$schema = $q->param('schema');
	$env = $q->param('r_env');
	$server_id = $q->param('db_server');
	print "<u>Clicking on the DB ID will populate the right side fields.</u><br>\n";
	#
	# Build while string
	#
	if(length($db_name) > 0) {
		$search_string = "lower(db_name) like lower('%$db_name%')";
		$a = 1;
	}
	if(length($narrative_name) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(narrative_name) like lower('%$narrative_name%')";
		} else {
			$search_string = "lower(narrative_name) like lower('%$narrative_name%')";
			$a = 1;
		}
	}
	if(length($db_type) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(db_type) like lower('%$db_type%')";
		} else {
			$search_string = "lower(db_type) like lower('%$db_type%')";
			$a = 1;
		}
	}
	if(length($schema) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(schema) like lower('%$schema%')";
		} else {
			$search_string = "lower(schema) like lower('%$schema%')";
			$a = 1;
		}
	}
	if(length($env) > 0) {
		if($a) {
			$search_string = $search_string . " and env='$env'";
		} else {
			$search_string = "env='$env'";
			$a = 1;
		}
	}
	if($server_id > 0) {
		if($a) {
			$search_string = $search_string . " and server_id=$server_id";
		} else {
			$search_string = "server_id=$server_id";
			$a = 1;
		}
	}
	if($a != 0) {
		$search_string = "where " . $search_string;
	} else {
		$search_string = "";
	}
	$insstr = "select db_name, narrative_name, db_type, schema, env, server_id, db_id from cmdb_db $search_string order by db_name";
        $getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>DB ID</th>\n";
	print "<th>DB Name</th>\n";
	print "<th>Narrative Name</th>\n";
	print "<th>DB Type</th>\n";
	print "<th>Schema</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server</th>\n";
	print "</tr>\n";
	while (($db_name, $narrative_name, $db_type, $schema, $env, $server_id, $db_id) = $getTbl->fetchrow()) {
		if(length($db_type) > 0) {
			if(!defined($server_id) || $server_id eq "") {
				$server_id = 0;
			}
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillDBForm('$db_name', '$narrative_name', '$db_type', '$schema', '$env', $server_id, $db_id)\">$db_id</a></td>\n";
			if(length($db_name) < 1) { $db_name = "&nbsp;"; }
			print "<td>$db_name</td>\n";
			if(length($narrative_name) < 1) { $narrative_name = "&nbsp;"; }
			print "<td>$narrative_name</td>\n";
			if(length($db_type) < 1) { $db_type = "&nbsp;"; }
			print "<td>$db_type</td>\n";
			if(length($schema) < 1) { $schema = "&nbsp;"; }
			print "<td>$schema</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";

} elsif(defined($q->param('l_search'))) {
	my $name;
	my $type;
	my $system_id;
	my $env;
	my $server;
	my $server_id;
	my $app_server;
	my $deploy_location;
	my $a = 0;
	$name = $q->param('l_name');
	$type = $q->param('l_type');
	$system_id = $q->param('l_system_id');
	$env = $q->param('l_env');
	$server = $q->param('l_server');
	$server_id = $q->param('l_system_id');
	$deploy_location = $q->param('l_deploy_location');
	$app_server = $q->param('l_app_server');
	$side = "L";
	print "<u>Clicking on the Env ID will populate the left side fields.</u><br>\n";
	#
	# Build while string
	#
	if(length($name) > 0) {
		$name =~ s/ /%/g;
		$search_string = "lower(comp_name) like lower('%$name%')";
		$a = 1;
	}
	if(length($system_id) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(system_id) like lower('%$system_id%')";
		} else {
			$search_string = "lower(system_id) like lower('%$system_id%')";
			$a = 1;
		}
	}
	if(length($app_server) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(app_server) like lower('%$app_server%')";
		} else {
			$search_string = "lower(app_server) like lower('%$app_server%')";
			$a = 1;
		}
	}
	if(length($type) > 0) {
		if($a) {
			$search_string = $search_string . " and comp_type='$type'";
		} else {
			$search_string = "comp_type='$type'";
			$a = 1;
		}
	}
	if(length($env) > 0) {
		if($a) {
			$search_string = $search_string . " and env='$env'";
		} else {
			$search_string = "env='$env'";
			$a = 1;
		}
	}
	if(length($deploy_location) > 0) {
		if($a) {
			$search_string = $search_string . " and lower(deploy_location) like lower('%$deploy_location%')";
		} else {
			$search_string = "lower(deploy_location) like lower('%$deploy_location%')";
			$a = 1;
		}
	}
	if($server_id > 0) {
		if($a) {
			$search_string = $search_string . " and server_id=$server_id";
		} else {
			$search_string = "server_id=$server_id";
			$a = 1;
		}
	}
	if($a != 0) {
		$search_string = "where " . $search_string . " and cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
	} else {
		$search_string = "where cmdb_comp_gen.comp_id=cmdb_comp_env.comp_id";
	}
        $getTbl = $dbh->prepare("select cmdb_comp_gen.comp_id, comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server, deploy_location from cmdb_comp_gen, cmdb_comp_env $search_string order by comp_name") or die "Error preparing statement: $DBI::errstr\n";
        $getTbl->execute();
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Env ID</th>\n";
	print "<th>Comp Name</th>\n";
	print "<th>Comp Type</th>\n";
	print "<th>System ID</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server</th>\n";
	print "<th>App Server</th>\n";
	print "</tr>\n";
	while (($comp_id, $comp_env_id, $comp_name, $comp_type, $system_id, $env, $server_id, $app_server, $deploy_location) = $getTbl->fetchrow()) {
		if(length($comp_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillUsedForm($comp_id, $comp_env_id, '$comp_name', '$comp_type', '$system_id', '$env', $server_id, '$app_server', '$deploy_location')\">$comp_env_id</a></td>\n";
			print "<td>$comp_name</td>\n";
			if(length($comp_type) < 1) { $comp_type = "&nbsp;"; }
			print "<td>$comp_type</td>\n";
			if(length($system_id) < 1) { $system_id = "&nbsp;"; }
			print "<td>$system_id</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($app_server) < 1) { $app_server = "&nbsp;"; }
			print "<td>$app_server</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";
	

	

} elsif ($inputtype eq 'Find Users') {
	my $db_id;
	my $db_name;
	$db_id = $q->param('db_id');
	$db_name = $q->param('db_name');
	if($db_id == 0 || $db_id eq "") {
		print "<b>No database ID available.  Please try again.</b>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}
	$insstr = "select cmdb_comp_gen.comp_id, cmdb_comp_env.comp_env_id, comp_name, comp_type, system_id, env, server_id, app_server from cmdb_comp_gen, cmdb_comp_env, cmdb_db_uses where db_id=$db_id and cmdb_db_uses.comp_env_id=cmdb_comp_env.comp_env_id and cmdb_comp_env.comp_id=cmdb_comp_gen.comp_id order by comp_env_id, comp_name, comp_type, env";
#print "<p><b>$insstr</b></p>\n";
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n"; $getTbl->execute();
	$side = "L";
	print "<p>$db_name is used by:";
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>Env ID</th>\n";
	print "<th>Comp Name</th>\n";
	print "<th>Comp Type</th>\n";
	print "<th>System ID</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server ID</th>\n";
	print "<th>App Server</th>\n";
	print "</tr>\n";
	while (($comp_id, $comp_env_id, $comp_name, $comp_type, $system_id, $env, $server_id, $app_server) = $getTbl->fetchrow()) {
		if(length($comp_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillUsedForm($comp_id, $comp_env_id, '$comp_name', '$comp_type', '$system_id', '$env', $server_id, '$app_server')\">$comp_env_id</a></td>\n";
			print "<td>$comp_name</td>\n";
			if(length($comp_type) < 1) { $comp_type = "&nbsp;"; }
			print "<td>$comp_type</td>\n";
			if(length($system_id) < 1) { $system_id = "&nbsp;"; }
			print "<td>$system_id</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			if(length($app_server) < 1) { $app_server = "&nbsp;"; }
			print "<td>$app_server</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";




} elsif ($inputtype eq 'Find Used By') {
	my $name;
	my $env_id;
	$name = $q->param('l_name');
	$verb = "uses";
	$env_id = $q->param('l_env_id');
	if($env_id == 0 || $env_id eq "") {
		print "<b>No Env ID available.  Please try again.</b>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}
	$insstr = "select db_name, narrative_name, db_type, schema, env, server_id, cmdb_db.db_id from cmdb_db, cmdb_db_uses where cmdb_db_uses.comp_env_id=$env_id and cmdb_db.db_id=cmdb_db_uses.db_id order by db_id";
	$getTbl = $dbh->prepare($insstr) or die "Error preparing statement: $DBI::errstr\n"; $getTbl->execute();
	$side = "R";
	print "<p>$name $verb:";
	#
	# Create/populate the table we got from
	# the user's search parameters.
	#
	print "<table border=1>\n";
	print "<tr>\n";
	print "<th>DB ID</th>\n";
	print "<th>DB Name</th>\n";
	print "<th>Narrative Name</th>\n";
	print "<th>DB Type</th>\n";
	print "<th>Schema</th>\n";
	print "<th>Environment</th>\n";
	print "<th>Server</th>\n";
	print "</tr>\n";
	while (($db_name, $narrative_name, $db_type, $schema, $env, $server_id, $db_id) = $getTbl->fetchrow()) {
		if(length($db_name) > 0) {
			print "<tr>\n";
			print "<td><a href=\"#\" onClick=\"fillDBForm('$db_name', '$narrative_name', '$db_type', '$schema', '$env', $server_id, $db_id)\">$db_id</a></td>\n";
			print "<td>$db_name</td>\n";
			if(length($narrative_name) < 1) { $narrative_name = "&nbsp;"; }
			print "<td>$narrative_name</td>\n";
			if(length($db_type) < 1) { $db_type = "&nbsp;"; }
			print "<td>$db_type</td>\n";
			if(length($schema) < 1) { $schema = "&nbsp;"; }
			print "<td>$schema</td>\n";
			if(length($env) < 1) { $env = "&nbsp;"; }
			print "<td>$env</td>\n";
			for($i = 0; $i <= $#server; $i++) {
				if($server_id == $server[$i]->{server_id}) {
					$server_name = "$server[$i]->{server_name}";
					last;
				}
			}
			if(length($server_name) < 1) { $server_name = "&nbsp;"; }
			print "<td>$server_name</td>\n";
			print "</tr>\n";
		}
	}
	$getTbl->finish;
	print "</table>\n";



} elsif ($inputtype eq 'Create New Relationship') {
	my $db_id = $q->param('db_id');
	my $env_id = $q->param('l_env_id');

	if($env_id == 0 || $env_id eq "" || $db_id == 0 || $db_id eq "") {
		print "<b>Missing Env ID or DB ID.  Please try again.</b>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "select db_id from cmdb_db_uses where db_id=$db_id and comp_env_id=$env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$rows = 0;
	while($this_db_id = $getTbl->fetchrow()) {
		$rows++;
	}
	$getTbl->finish;
	if($rows > 0) {
		print "<p><b>There is already a relationship between DB ID $db_id and ENV ID $env_id.  No changes made to table.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "insert into cmdb_db_uses values ($db_id, $env_id)";
	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Insert into CMDB_DB_USES completed.</p>\n";
	} else {
		print "<p>Insert into CMDB_DB_USES failed:</p>\n";
		print "<p>$insstr</p>\n";
	}

} elsif ($inputtype eq 'Remove Relationship' || $do_delete eq 'yes') {
	my $db_id = $q->param('db_id');
	my $env_id = $q->param('l_env_id');
	my $rows = 0;
	my $this_db_id = 0;

	if($env_id == 0 || $env_id eq "" || $db_id == 0 || $db_id eq "") {
		print "<b>Missing Env ID or DB ID.  Please try again.</b>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

        if($db_id < 1 || $db_id == "") {
                print"<p><b>DB ID is zero.  Can't delete.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }
        if($env_id < 1 || $env_id == "") {
                print"<p><b>ENV ID is zero.  Can't delete.</b></p>\n";
                print $q->end_html();
                $dbh->disconnect;
                exit 0;
        }

	$insstr = "select db_id from cmdb_db_uses where db_id=$db_id and comp_env_id=$env_id";
	$getTbl = $dbh->prepare($insstr);
	$getTbl->execute();
	$rows = 0;
	while($this_db_id = $getTbl->fetchrow()) {
		$rows++;
	}
	$getTbl->finish;
	if($rows == 0) {
		print "<p><b>There is no relationship between DB ID $db_id and ENV ID $env_id.  No changes made to table.</b></p>\n";
		print $q->end_html();
		$dbh->disconnect;
		exit 0;
	}

	$insstr = "delete from cmdb_db_uses where db_id=$db_id and comp_env_id=$env_id";

	#print "<br>$insstr<br>\n";

	$getTbl = $dbh->prepare($insstr);
	$i = $getTbl->execute();
	$getTbl->finish;
	if($i != 0) {
		print "<p>Delete from CMDB_DB_USES completed.</p>\n";
	} else {
		print "<p><b>Delete from CMDB_DB_USES failed.</b></p>\n";
		print "<p>$insstr</p>\n";
	}

}
print $q->end_html();

#$dbh->commit();
$dbh->disconnect;

exit 0;


